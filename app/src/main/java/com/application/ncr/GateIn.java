package com.application.ncr;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class GateIn extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    SessionManager session;
    String Username, userId, DesingId;
    DrawerLayout drawer;
    TextInputEditText vehicleNumerET, driverNameET, driverNumberET,driver_lisence_number_ET;
    TextInputLayout veh_number_input_TI, driver_name_TI, driver_number_TI,driver_lisence_number_TI;
    Button submit;
    String driverName, vehicleNumber, driverNumber, exceptionMsg;
    private ProgressDialog pDialog;
    String vendorId,indentId,vehicleId,receiptId,vehNum,driverLicense;

    Dialog dialog;
    final Context context = this;

    //    camera activity
    LinearLayout podCapture;
    GridView gridView;
    int MY_CAMERA_PERMISSION_CODE = 100, CAMERA_REQUEST = 1888;
    byte[] CustomerSignature = null;
    ArrayList imageString = new ArrayList();
    int count = 0;
    ArrayList<ColorItem> CamraTakeImgList = new ArrayList<>();
    String billImage = null;
    Button photoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gate_in);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);

        Intent intent = getIntent();
        vendorId = intent.getStringExtra("vendor_id");
        vehicleId = intent.getStringExtra("vehicleId");
        indentId = intent.getStringExtra("indentId");
        receiptId = intent.getStringExtra("receiptId");
        vehNum = intent.getStringExtra("vehicleNum");

        System.out.println("vendorId=="+vendorId);

        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Feild Executive");
        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code

        vehicleNumerET = (TextInputEditText) findViewById(R.id.editText1);
        vehicleNumerET.setText(vehNum);
        vehicleNumerET.setFocusable(false);
        driverNameET = (TextInputEditText) findViewById(R.id.editText2);
        driverNumberET = (TextInputEditText) findViewById(R.id.editText3);
        driver_lisence_number_ET = (TextInputEditText) findViewById(R.id.editText4);
        submit = (Button) findViewById(R.id.submit);

        veh_number_input_TI = findViewById(R.id.veh_number_input);
        driver_name_TI = findViewById(R.id.driver_name);
        driver_number_TI = findViewById(R.id.driver_number);

        podCapture = (LinearLayout) findViewById(R.id.podCapture);
        photoButton = (Button) findViewById(R.id.button1);
        gridView = (GridView) findViewById(R.id.gridView);


        photoButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);

                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });


        driverNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
//                    System.out.println("iam here");
                    if (driverNameET.getText().length() == 0 && driverNameET.getText().toString().equals("")) {
                        System.out.println("iam here 1");
                        driver_name_TI.setErrorEnabled(true);
                        driver_name_TI.setError("error");


                    } else {
                        driver_name_TI.setError(null);
                        driver_name_TI.setErrorEnabled(false);
                    }
                }
            }
        });

        vehicleNumerET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
//                    System.out.println("iam here");
                    if (vehicleNumerET.getText().length() == 0 && vehicleNumerET.getText().toString().equals("")) {
                        System.out.println("iam here 1");
                        veh_number_input_TI.setErrorEnabled(true);
                        veh_number_input_TI.setError("error");


                    } else {
                        veh_number_input_TI.setError(null);
                        veh_number_input_TI.setErrorEnabled(false);
                    }
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Check if text controls are not empty

                if (vehicleNumerET.getText().length() != 0 && vehicleNumerET.getText().toString() != "") {
                    if (driverNameET.getText().length() != 0 && driverNameET.getText().toString() != "") {
                        if (driverNumberET.getText().length() != 0 && driverNumberET.getText().toString() != "") {
                        if (driver_lisence_number_ET.getText().length() != 0 && driver_lisence_number_ET.getText().toString() != "") {


//                            if (imageString.size() > 0) {


                            vehicleNumber = vehicleNumerET.getText().toString();
                            driverName = driverNameET.getText().toString();
                            driverNumber = driverNumberET.getText().toString();
                            driverLicense = driver_lisence_number_ET.getText().toString();


                            new submitData().execute();
//                            } else {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        Toast.makeText(getApplicationContext(), "Capture Atleast One Image", Toast.LENGTH_LONG).show();
//                                    }
//                                });
//                            }


                        }
                        else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Please Enter Driver License Number", Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                        }//mobilenumber is empty
                        else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Please Enter Driver Number", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }//If vehicle number  text control is empty
                    else {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Please Enter Driver Name", Toast.LENGTH_LONG).show();
                            }
                        });


                    }//If  drivername text control is empty
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Please Enter Vehicle Number", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            session.logoutUser();
        }

        return true;
    }

    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }


//    public class submitData extends AsyncTask<String, Void, Void> {
//        String obj = null;
//        String jsonStr = null;
//
//        @Override
//        //Make Progress Bar visible
//        protected void onPreExecute() {
//            pDialog = new ProgressDialog(GateIn.this);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(String... params) {
//
//            try {
//
//                JSONObject jResult = new JSONObject();
//                JSONArray jArray = new JSONArray();
//                JSONArray main = new JSONArray();
//                for (int i = 0; i < imageString.size(); i++) {
//                    JSONObject jGroup = new JSONObject();// /sub Object
//                    jGroup.put("image", imageString.get(i));
//                    jArray.put(jGroup);
//                }
//
//                jResult.put("vehImage", jArray);
//                main.put(jResult);
//                obj = main.toString();
//                System.out.println("pod_json===" + main);
//                jsonStr = WebService.updateVehicleGateIn(driverName, driverNumber, vehicleNumber, userId, obj);
//                System.out.println("json_string_output====" + jsonStr);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
//            if (jsonStr != null) {
//                try {
//                    JSONArray jsonArray = new JSONArray(jsonStr);
//                    JSONObject jsonObject = jsonArray.getJSONObject(0);
//                    exceptionMsg = jsonObject.getString("status");
//                    if ("1".equalsIgnoreCase(exceptionMsg)) {
//                        Intent intObj = new Intent(GateIn.this, MainActivity.class);
//                        startActivity(intObj);
//                    } else {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Toast.makeText(getApplicationContext(), "Something went wrong please try again", Toast.LENGTH_LONG).show();
//                            }
//                        });
//                        Intent intObj = new Intent(GateIn.this, GateIn.class);
//                        startActivity(intObj);
//                    }
//
//                } catch (final JSONException e) {
//                    System.out.println("JSONException=========" + e);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
//                        }
//                    });
//                }
//            } else {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//        }
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {


            try {
                count++;
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                CustomerSignature = bos.toByteArray();
                billImage = Base64.encodeToString(CustomerSignature, Base64.NO_WRAP);
                imageString.add(billImage);
                ColorItem select = new ColorItem();
                select.setBitmap(bitmap);
                select.setColorName("Pic" + " " + count);
                CamraTakeImgList.add(select);
                SelectedImageAdapter seletimg = new SelectedImageAdapter(GateIn.this, CamraTakeImgList);
                gridView.setAdapter(seletimg);

            } catch (NullPointerException e) {

                e.printStackTrace();

            }

        }
    }



    public class submitData extends AsyncTask<String, Void, Void> {
        String obj = null;
        String jsonStr = null;

        @Override
        //Make Progress Bar visible
        protected void onPreExecute() {
            pDialog = new ProgressDialog(GateIn.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                JSONObject podPic = new JSONObject();
                JSONObject jResult = new JSONObject();
                JSONArray jArray = new JSONArray();
                JSONArray main = new JSONArray();
                for (int i = 0; i < imageString.size(); i++) {
                    podPic.put("Image"+i, imageString.get(i));
                    jArray.put(podPic);

                }
                jResult.put("podImage", jArray);
                main.put(jResult);
                obj = main.toString();

                String status_id = "1";
                jsonStr = WebService.updateVehicleGateIn(driverName, driverNumber, vehicleNumber, userId, obj,status_id,vendorId,vehicleId,indentId,receiptId,driverLicense);
                System.out.println("json_string_output====" + jsonStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    exceptionMsg = jsonObject.getString("status");
                    if ("1".equalsIgnoreCase(exceptionMsg)) {
                        Intent intObj = new Intent(GateIn.this, Dashboard.class);
                        startActivity(intObj);
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Something went wrong please try again", Toast.LENGTH_LONG).show();
                            }
                        });
                        Intent intObj = new Intent(GateIn.this, GateIn.class);
                        startActivity(intObj);
                    }

                } catch (final JSONException e) {
                    System.out.println("JSONException=========" + e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }











//    @Override
//    public void onBackPressed() {
//
//
//        dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
////        dialog.setTitle("Leave application?");
////        dialog.setMessage("Are you sure you want to leave the application?");
//        dialog.setContentView(R.layout.custom_alert_dialouge);
//        dialog.setCancelable(false);
//        Button yesButton = (Button) dialog.findViewById(R.id.yes);
//        Button noButton = (Button) dialog.findViewById(R.id.no);
//        TextView title = (TextView) dialog.findViewById(R.id.title);
//        TextView message = (TextView) dialog.findViewById(R.id.message);
//        title.setText("Leave application?");
//        message.setText("Are you sure you want to leave the application?");
//
//
//        yesButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                session.logoutUser();
//            }
//        });
//        noButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
////        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
////                WareHouseDashboard.this);
////        alertDialog.setTitle("Leave application?");
////        alertDialog.setMessage("Are you sure you want to leave the application?");
////        alertDialog.setPositiveButton("YES",
////                new DialogInterface.OnClickListener() {
////                    public void onClick(DialogInterface dialog, int which) {
////                        session.logoutUser();
////                    }
////                });
////
////        alertDialog.setNegativeButton("NO",
////                new DialogInterface.OnClickListener() {
////                    public void onClick(DialogInterface dialog, int which) {
////                        dialog.cancel();
////                    }
////                });
////        alertDialog.show();
//    }
}

