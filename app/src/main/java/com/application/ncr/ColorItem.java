package com.application.ncr;

import android.graphics.Bitmap;

public class ColorItem {
    private String ColorName;

    private Bitmap bitmap;

    public Bitmap getBitmap() {

        return bitmap;

    }



    public void setBitmap(Bitmap bitmap) {

        this.bitmap = bitmap;

    }



    public String getColorName() {

        return ColorName;

    }



    public void setColorName(String colorName) {

        ColorName = colorName;

    }
}

