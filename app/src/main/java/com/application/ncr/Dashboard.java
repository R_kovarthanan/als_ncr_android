package com.application.ncr;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Dashboard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    SessionManager session;
    DrawerLayout drawer;
    PieChart pieChart;
    CardView audit, gateIn, gateOut, dispatch,test;
    Dialog dialog;
    final Context context = this;
    String Username, userId, DesingId;
    TextView gi_count_tv,audit_count_tv,dispatch_count_tv,go_count_tv,vendor_name_tv;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dash_board);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);

        new getDashboardCount().execute();


        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Feild Executive");
        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
//        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
//        nav_dashboard.setVisible(false);

        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code


        gateIn = findViewById(R.id.gateIn);
        audit = findViewById(R.id.audit);
        dispatch = findViewById(R.id.dispatch);
        gateOut = findViewById(R.id.gateOut);
        gi_count_tv = findViewById(R.id.gate_in_count);
        audit_count_tv = findViewById(R.id.audit_in_count);
        dispatch_count_tv = findViewById(R.id.dispatch_in_count);
        go_count_tv = findViewById(R.id.gate_out_count);
        vendor_name_tv = findViewById(R.id.vendor_name);


        gateIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intObj = new Intent(Dashboard.this, GateInVehicleListActivity.class);
//                intObj.putExtra("username", username);
                startActivity(intObj);
            }

        });

        audit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intObj = new Intent(Dashboard.this, AuditVehicleListActivity.class);
//                intObj.putExtra("username", username);
                startActivity(intObj);
            }

        });
        dispatch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intObj = new Intent(Dashboard.this, DispatchVehicleListActivity.class);
//                intObj.putExtra("username", username);
                startActivity(intObj);
            }

        });

        gateOut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intObj = new Intent(Dashboard.this, GateOutVehicleListActivity.class);
//                intObj.putExtra("username", username);
                startActivity(intObj);
            }

        });
//        test.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Intent intObj = new Intent(Dashboard.this, Test.class);
////                intObj.putExtra("username", username);
//                startActivity(intObj);
//            }

//        });



        pieChart = findViewById(R.id.piechart);
        pieChart.addPieSlice(
                new PieModel(
                        "Audit",
                        0,
                        Color.parseColor("#FFA726")));
        pieChart.addPieSlice(
                new PieModel(
                        "GateIn",
                        25,
                        Color.parseColor("#66BB6A")));
        pieChart.addPieSlice(
                new PieModel(
                        "Dispatch",
                        25,
                        Color.parseColor("#AABB6A")));
        pieChart.addPieSlice(
                new PieModel(
                        "GateOut",
                        25,
                        Color.parseColor("#BBBBBB")));

        pieChart.startAnimation();


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        new getDashboardCount().execute();
    }





    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            Intent mainIntent = new Intent(Dashboard.this, CheckLoginActivity.class);
            startActivity(mainIntent);
        }
//            if(DesingId.equals("1042")){
//
//            }
        return true;
    }


    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }



    public class getDashboardCount extends AsyncTask<String, String, String> {
        String getDashboardCount;

        protected void onPreExecute() {
            pDialog = new ProgressDialog(Dashboard.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        public String doInBackground(String... params) {
            getDashboardCount = WebService.getDashboardCount(userId);
            System.out.println("getDashboardCount====" + getDashboardCount);
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            if (getDashboardCount != null) {
                try {


                    JSONArray jsonArray = new JSONArray(getDashboardCount);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    gi_count_tv.setText(jsonObject.getString("gate_in_count"));
                    audit_count_tv.setText(jsonObject.getString("audit_count"));
                    dispatch_count_tv.setText(jsonObject.getString("dispatch_count"));
                    go_count_tv.setText(jsonObject.getString("gate_out_count"));
                    vendor_name_tv.setText(jsonObject.getString("vendor_name"));



                } catch (final JSONException e) {
                    System.out.println("JSONException=========" + e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Something went wrong,please try again", Toast.LENGTH_LONG).show();
                        }
                    });
//                    statusTV.setText("Invalid Data");

                }
            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }


        }
    }

    @Override
    public void onBackPressed() {


        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert_dialouge);
        dialog.setCancelable(false);
        Button yesButton = (Button) dialog.findViewById(R.id.yes);
        Button noButton = (Button) dialog.findViewById(R.id.no);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView message = (TextView) dialog.findViewById(R.id.message);
        title.setText("Leave application?");
        message.setText("Are you sure you want to leave the application?");


        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.logoutUser();
            }
        });
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }


}
