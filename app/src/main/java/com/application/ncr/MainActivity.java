package com.application.ncr;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    String Username, userId, DesingId;
    SessionManager session;
    boolean isPermitted;
    LinearLayout linlaHeaderProgress;
    String dedicateTripStatus, exceptionOccur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getApplicationContext());

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        session.checkLogin();
        if (session.isLoggedIn()) {
            System.out.println("yes ur loged in");
            HashMap<String, String> user = session.getUserDetails();
            Username = user.get(SessionManager.KEY_NAME);
            userId = user.get(SessionManager.KEY_USERID);
            DesingId = user.get(SessionManager.KEY_DESIG);
            System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);
        } else {
            System.out.println("no ur not logged in");
            Intent mainIntent = new Intent(MainActivity.this, CheckLoginActivity.class);
            startActivity(mainIntent);
        }

        if (hasConnection(MainActivity.this)) {

        } else {
            showNetDisabledAlertToUser(MainActivity.this);
        }
//        checkRunTimePermission();

        if (DesingId != null && !DesingId.equals("null")) {

//            if (DesingId.equals("1046")) {

                Intent intObj = new Intent(MainActivity.this, Dashboard.class);
                startActivity(intObj);
//            }else{
//Intent mainIntent = new Intent(MainActivity.this, CheckLoginActivity.class);
//                            startActivity(mainIntent);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(getApplicationContext(), "No screen for this user", Toast.LENGTH_LONG).show();
//                        }
//                    });
//
//            }


//            if (DesingId.equals("1034")) {
////                new Details().execute();
//                Intent mainIntent = new Intent(MainActivity.this, TripListActivity.class);
//                startActivity(mainIntent);
//            }
//
//            if (DesingId.equals("1058")) {
//                Intent mainIntent = new Intent(MainActivity.this, FuelFilling.class);
//                startActivity(mainIntent);
//            }
        }

        setContentView(R.layout.activity_main);
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
    }

    public boolean hasConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }
        return false;
    }

    public static void showNetDisabledAlertToUser(final Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setMessage("No Internet Connection")
                .setTitle("No Internet Connection");

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {
            // if already permition granted
            // PUT YOUR ACTION (Like Open cemara etc..)
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean openActivityOnce = true;
        boolean openDialogOnce = true;
        if (requestCode == 11111) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView(MainActivity.this);
                        }
                    }
                }
            }

//            if (isPermitted)
//                if (isPermissionFromGallery)
//                    openGalleryFragment();
        }
    }

    private void alertView(final Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context, AlertDialog.THEME_TRADITIONAL);

        dialog.setTitle("Permission Denied")
                .setInverseBackgroundForced(true)
                //.setIcon(R.drawable.ic_info_black_24dp)
                .setMessage("Without those permission the app is unable to save your profile. App needs to save profile image in your external storage and also need to get profile image from camera or external storage.Are you sure you want to deny this permission?")

                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }
                })
                .setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        checkRunTimePermission();

                    }
                }).show();
    }


//    public class Details extends AsyncTask<String, String, String> {
//
//        String tripDetails = null;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            linlaHeaderProgress.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        public String doInBackground(String... params) {
//            tripDetails = WebService.getTripList(userId);
//            return tripDetails;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            linlaHeaderProgress.setVisibility(View.GONE);
//            if (tripDetails != null) {
//
//
//                try {
//                    JSONArray array = new JSONArray(tripDetails);
//
//                    for (int i = 0; i < array.length(); i++) {
//                        JSONObject jsonObj = array.getJSONObject(i);
//                        exceptionOccur = jsonObj.getString("expOccur");
//                        dedicateTripStatus = jsonObj.getString("dedicateTripStatus");
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                try {
//
//                    if(exceptionOccur.equalsIgnoreCase("N")) {
//                        if (dedicateTripStatus.equals("0") || dedicateTripStatus.equals("1")) {
//                            Intent mainIntent = new Intent(MainActivity.this, TripDetails.class);
//                            startActivity(mainIntent);
//                        } else {
//                            Intent mainIntent = new Intent(MainActivity.this, RunSheetActivity.class);
//                            startActivity(mainIntent);
//                        }
//                    }else{
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Toast.makeText(getApplicationContext(), "Something went wrong,please try again", Toast.LENGTH_LONG).show();
//                            }
//                        });
//                    }
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                }
//            }else{
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//
//        }
//    }


}