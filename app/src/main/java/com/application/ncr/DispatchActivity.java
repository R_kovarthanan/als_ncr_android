package com.application.ncr;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DispatchActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    SessionManager session;
    DrawerLayout drawer;
    private ProgressDialog pDialog;
    DispatchAdapter runSheetAdapter;
    ListView lv;
    TextInputEditText etSearch;
    TextInputLayout searchbar;
    //    String username;
    String Username, userId, DesingId;
    String vendorId, indentId, vehicleId,consignmentId,receiptId,ewaybillNum,ewaybillDate,ewaybillType,tvp,consignorId;
//    int invoiceListSize;
    CardView noinvoice;
    Button addinv, submit;
    String exceptionMsg;
    ArrayList<DispatchDetailsTO> invoiceList = new ArrayList<DispatchDetailsTO>();
    Dialog dialog;
    final Context context = this;
    int checkedStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.dispatch_activity);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);

        Intent intent = getIntent();
        vendorId = intent.getStringExtra("vendor_id");
        vehicleId = intent.getStringExtra("vehicleId");
        indentId = intent.getStringExtra("indentId");
        receiptId = intent.getStringExtra("receiptId");
        ewaybillNum = intent.getStringExtra("ewaybillNum");
        ewaybillDate = intent.getStringExtra("ewaybillDate");
        ewaybillType = intent.getStringExtra("ewaybillType");
        tvp = intent.getStringExtra("tvp");
        consignorId = intent.getStringExtra("consignorId");


        System.out.println("ewaybillType==="+ewaybillType);
        System.out.println("consignorId in dispatchactivity==="+consignorId);
//        consignmentId = intent.getStringExtra("consignmentId");

        new getInvoiceList().execute();


        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Feild Executive");
        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
//        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
//        nav_dashboard.setVisible(false);

        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code


        lv = (ListView) findViewById(R.id.homeListView);
        noinvoice = (CardView) findViewById(R.id.noinvoicecard);
        addinv = (Button) findViewById(R.id.addinv);
        submit = (Button) findViewById(R.id.submit);
        System.out.println("invoiceList.size()===="+invoiceList.size());


        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("invoiceList.size() inside submit===="+invoiceList.size());
                if (invoiceList.size() > 0) {

                    dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.custom_alert_dialouge);
                    dialog.setCancelable(false);
                    Button yesButton = (Button) dialog.findViewById(R.id.yes);
                    Button noButton = (Button) dialog.findViewById(R.id.no);
                    Switch toggle = dialog.findViewById(R.id.switch2);
                    yesButton.setText("Dispatch");
                    noButton.setText("cancel");
                    TextView title = (TextView) dialog.findViewById(R.id.title);
                    TextView message = (TextView) dialog.findViewById(R.id.message);
                    title.setText("Alert");
                    message.setText("Please Verify the invoices before dispatching");


                    yesButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            if (toggle.isChecked()){
                                checkedStatus = 1;
                            }
                            else {
                                checkedStatus = 0;
                            }
                            new updateDispatchDetails().execute();
                        }
                    });
                    noButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
//                    new updateDispatchDetails().execute();
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Please add invoice details", Toast.LENGTH_LONG).show();
                        }
                    });
                }


            }
        });

        addinv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intObj = new Intent(DispatchActivity.this, InvoiceDetails.class);
                intObj.putExtra("vehicleId", vehicleId);
                intObj.putExtra("vendor_id", vendorId);
                intObj.putExtra("indentId", indentId);
                intObj.putExtra("receiptId", receiptId);
                intObj.putExtra("ewaybillNum", ewaybillNum);
                intObj.putExtra("ewaybillDate", ewaybillDate);
                intObj.putExtra("ewaybillType", ewaybillType);
                intObj.putExtra("tvp", tvp);
                intObj.putExtra("consignorId", consignorId);
                startActivity(intObj);

            }
        });


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            Intent mainIntent = new Intent(DispatchActivity.this, CheckLoginActivity.class);
            startActivity(mainIntent);
        }
//            if(DesingId.equals("1042")){
//
//            }
        return true;
    }


    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }





    public class updateDispatchDetails extends AsyncTask<String, String, String> {

        String tripDetails = null;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(DispatchActivity.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String status_id = "3";
            tripDetails = WebService.updateDispatchDetails(vehicleId, vendorId, indentId, userId,status_id,consignmentId,receiptId,checkedStatus);
            System.out.println("tripDetails=========="+tripDetails);
            return tripDetails;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (tripDetails != null) {
                try {
                    JSONArray jsonArray = new JSONArray(tripDetails);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    exceptionMsg = jsonObject.getString("status");
                    if ("1".equalsIgnoreCase(exceptionMsg)) {
                        Intent intObj = new Intent(DispatchActivity.this, Dashboard.class);
                        startActivity(intObj);
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Something went wrong please try again", Toast.LENGTH_LONG).show();
                            }
                        });

                    }

                } catch (final JSONException e) {
                    System.out.println("JSONException=========" + e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }

        }


    }

    public class getInvoiceList extends AsyncTask<String, String, String> {

        String tripDetails = null;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(DispatchActivity.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            tripDetails = WebService.getInvoiceList(receiptId);
            return tripDetails;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (tripDetails != null) {

                try {
                    JSONArray array = new JSONArray(tripDetails);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObj = array.getJSONObject(i);
                        DispatchDetailsTO runSheet = new DispatchDetailsTO(
                                jsonObj.getString("invoiceNum"),
                                jsonObj.getString("gcn"),
                                jsonObj.getString("eway_bill_num"),
                                jsonObj.getString("location"),
                                jsonObj.getString("consignee"),
                                jsonObj.getString("tvp_dealer"),
                                jsonObj.getString("bikeUnits"),
                                jsonObj.getString("scooterUnits"),
                                jsonObj.getString("inv_id")


                        );
                        invoiceList.add(runSheet);
                    }

                    if (invoiceList.size() == 0) {
                        noinvoice.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("exeption===="+ e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                    });
                }

                runSheetAdapter = new DispatchAdapter(DispatchActivity.this, invoiceList,indentId,receiptId);
                lv.setAdapter(runSheetAdapter);
            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }


        }


    }

    @Override
    public void onBackPressed() {
        Intent intObj = new Intent(DispatchActivity.this, DispatchVehicleListActivity.class);
//                intObj.putExtra("username", username);
        startActivity(intObj);
    }


}
