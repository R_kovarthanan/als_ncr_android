package com.application.ncr;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PostLoading extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ProgressDialog pDialog;
    Boolean ischecked = false;
    ListView lv;

    CheckListAdapter runSheetAdapter;
    ArrayList<CheckListTO> invoiceList = new ArrayList<CheckListTO>();

    Context context;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PostLoading(Context context) {
        // Required empty public constructor
        this.context = context;
    }

//    public static PostLoading newInstance(String param1, String param2) {
//        PostLoading fragment = new PostLoading();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        new getAuditForm().execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_post_loading, container, false);
        View rootView=inflater.inflate(R.layout.fragment_post_loading,container,false);
        lv = (ListView) rootView.findViewById(R.id.homeListView);

        return rootView;
    }

    public class getAuditForm extends AsyncTask<String, String, String> {

        String getAuditForm = null;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }



        @Override
        protected String doInBackground(String... strings) {

            getAuditForm = WebService.getAuditForm();
            System.out.println("getAuditForm===="+getAuditForm);
            return getAuditForm;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (getAuditForm != null) {

                try {
                    JSONArray array = new JSONArray(getAuditForm);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObj = array.getJSONObject(i);
                        CheckListTO runSheet = new CheckListTO(
                                jsonObj.getString("audit_id"),
                                jsonObj.getString("auditData"), ischecked
                        );

                        invoiceList.add(runSheet);
                    }

//                    for(int i=0; i<invoiceList.size();i++){
//                        System.out.println("auditid=="+invoiceList.get(i).getAuditId());
//                        System.out.println("auditData=="+invoiceList.get(i).getAuditData());
//                        System.out.println("isChecked=="+invoiceList.get(i).getIsChecked());
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("exeption====" + e);



                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();


                }

                runSheetAdapter = new CheckListAdapter(context, invoiceList);
                lv.setAdapter(runSheetAdapter);
            } else {


                Toast.makeText(context, "Could't get data from the server", Toast.LENGTH_LONG).show();
            }

        }


    }



}