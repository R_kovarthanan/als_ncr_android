package com.application.ncr;

public class DispatchDetailsTO {
    String invoiceNum = null;
    String gcn = null;
    String eway_bill_num = null;
    String location = null;
    String consignee = null;
    String tvp_dealer = null;
    String bikeUnits = null;
    String scooterUnits = null;
    String inv_id = null;

    public DispatchDetailsTO(String invoiceNum, String gcn, String eway_bill_num, String location, String consignee, String tvp_dealer, String bikeUnits,String scooterUnits,String inv_id) {
        this.invoiceNum = invoiceNum;
        this.gcn = gcn;
        this.eway_bill_num = eway_bill_num;
        this.location = location;
        this.consignee = consignee;
        this.tvp_dealer = tvp_dealer;
        this.bikeUnits = bikeUnits;
        this.scooterUnits = scooterUnits;
        this.inv_id = inv_id;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getGcn() {
        return gcn;
    }

    public void setGcn(String gcn) {
        this.gcn = gcn;
    }

    public String getEway_bill_num() {
        return eway_bill_num;
    }

    public void setEway_bill_num(String eway_bill_num) {
        this.eway_bill_num = eway_bill_num;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getTvp_dealer() {
        return tvp_dealer;
    }

    public void setTvp_dealer(String tvp_dealer) {
        this.tvp_dealer = tvp_dealer;
    }

    public String getBikeUnits() {
        return bikeUnits;
    }

    public void setBikeUnits(String bikeUnits) {
        this.bikeUnits = bikeUnits;
    }

    public String getScooterUnits() {
        return scooterUnits;
    }

    public void setScooterUnits(String scooterUnits) {
        this.scooterUnits = scooterUnits;
    }

    public String getInv_id() {
        return inv_id;
    }

    public void setInv_id(String inv_id) {
        this.inv_id = inv_id;
    }
}
