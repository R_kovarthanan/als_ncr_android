package com.application.ncr;

public class VehicleListTO {
    String vehicleNumber = null;
    String vendorName = null;
    String vendorId = null;
    String vehicleId = null;
    String indentId = null;
    String consignmentId = null;
    String receiptId = null;
    String receiptDate = null;

    public VehicleListTO(String vehicleNumber, String vendorName, String vendorId, String vehicleId, String indentId, String consignmentId, String receiptId,String receiptDate) {
        this.vehicleNumber = vehicleNumber;
        this.vendorName = vendorName;
        this.vendorId = vendorId;
        this.vehicleId = vehicleId;
        this.indentId = indentId;
        this.consignmentId = consignmentId;
        this.receiptId = receiptId;
        this.receiptDate = receiptDate;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getIndentId() {
        return indentId;
    }

    public void setIndentId(String indentId) {
        this.indentId = indentId;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }
}
