package com.application.ncr;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

public class CheckListAdapter extends BaseAdapter {
    Context context;
    ArrayList<CheckListTO> runSheetList;
    ArrayList<CheckListTO> runSheetListDisplayed;
    //    TextView audit_data_tv;
    CheckBox checkBox;

    public CheckListAdapter(Context context, ArrayList<CheckListTO> runSheetList) {
        this.context = context;
//        this.runSheetList = runSheetList;
        this.runSheetListDisplayed = runSheetList;
    }

    @Override
    public int getCount() {
        return runSheetListDisplayed.size();
    }

    @Override
    public Object getItem(int i) {
        return runSheetListDisplayed.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.checklist_adapter, null);

            holder.audit_data_tv = (TextView) convertView.findViewById(R.id.audit_data);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);


            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        holder.checkBox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Integer pos = (Integer) holder.checkBox.getTag();
//
//                if (runSheetListDisplayed.get(pos).getIsChecked()) {
//
//                    runSheetListDisplayed.get(pos).setIsChecked(false);
//
//                } else {
//                    runSheetListDisplayed.get(pos).setIsChecked(true);
//                }
//            }
//        });

        CheckListTO runSheet = runSheetListDisplayed.get(position);
        holder.audit_data_tv.setText(runSheet.getAuditData());


        holder.checkBox.setTag(position);
        holder.checkBox.setChecked(true);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (runSheetListDisplayed.get(position).getIsChecked()) {
//
                    runSheetListDisplayed.get(position).setIsChecked(false);

                } else {
                    runSheetListDisplayed.get(position).setIsChecked(true);
                }

            }

        });
//        if(holder.checkBox.isChecked()){
//            runSheetListDisplayed.get(position).setIsChecked(false);
//        }else {
//            runSheetListDisplayed.get(position).setIsChecked(true);
//        }


        return convertView;
    }



    private class ViewHolder {
        CheckBox checkBox;
        private TextView audit_data_tv;

    }


}
