package com.application.ncr;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SelectedImageAdapter extends BaseAdapter {


    private Context context;
    ArrayList myList = new ArrayList();

    private ArrayList<ColorItem> arraylist;

    int resorce;

    public SelectedImageAdapter(Context context, ArrayList<ColorItem> myList) {

        this.context = context;

        this.myList = myList;

        this.arraylist = new ArrayList<ColorItem>();

        this.arraylist.addAll(myList);



    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public ColorItem getItem(int position) {
        return (ColorItem) myList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ColorItem speciitem = getItem(position);

        final ViewHolder holder;

        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) context

                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);

            convertView = infalInflater.inflate(R.layout.selecteditem, null);

        }

        holder = new ViewHolder();

        try {



            holder.colors = (ImageView) convertView.findViewById(R.id.colors);

            holder.colorsname = (TextView)
                    convertView.findViewById(R.id.color_text);

            holder.colors.setImageBitmap(speciitem.getBitmap());

            holder.colorsname.setText(speciitem.getColorName());





        } catch (Exception e) {

            e.printStackTrace();

        }

        convertView.setTag(holder);

        return convertView;
    }


    static class ViewHolder {



        TextView colorsname, bagcount;

        ImageView colors;

    }
}
