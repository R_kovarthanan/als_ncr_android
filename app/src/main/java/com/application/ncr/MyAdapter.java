package com.application.ncr;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class MyAdapter extends FragmentPagerAdapter {


    Context context;
    int totalTabs;
    public MyAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PreLoading preLoading = new PreLoading(context);
                return preLoading;
            case 1:
                PostLoading postLoading = new PostLoading(context);
                return postLoading;
//                Cricket cricketFragment = new Cricket();
//                return cricketFragment;
//            case 2:
//                PreLoading preLoading2 = new PreLoading(context);
//                return preLoading2;
//                NBA nbaFragment = new NBA();
//                return nbaFragment;
//            case 3:
//                Pending pending = new Pending();
//                return pending;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}
