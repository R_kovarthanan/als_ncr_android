package com.application.ncr;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GateInVehicleListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    SessionManager session;
    DrawerLayout drawer;
    private ProgressDialog pDialog;
    GateInVehicleListAdapter runSheetAdapter;
    ListView lv;
    TextInputEditText etSearch;
    TextInputLayout searchbar;
    //    String username;
    String Username, userId, DesingId;
    CardView noinvoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Intent intent = getIntent();
//        username = intent.getStringExtra("username");

        setContentView(R.layout.gatein_vehicle_list);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);
        new getVehicleList().execute();


        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Feild Executive");
        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
//        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
//        nav_dashboard.setVisible(false);

        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code


        lv = (ListView) findViewById(R.id.homeListView);
        etSearch = (TextInputEditText) findViewById(R.id.etSearch);
        searchbar = (TextInputLayout) findViewById(R.id.userNameInput);
        noinvoice = (CardView) findViewById(R.id.noinvoicecard);

//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // Call back the Adapter with current character to Filter
//                runSheetAdapter.getFilter().filter(s.toString());
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            Intent mainIntent = new Intent(GateInVehicleListActivity.this, CheckLoginActivity.class);
            startActivity(mainIntent);
        }
//            if(DesingId.equals("1042")){
//
//            }
        return true;
    }


    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }



    public class getVehicleList extends AsyncTask<String, String, String> {

        String tripDetails = null;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(GateInVehicleListActivity.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String status_id = "0";
            tripDetails = WebService.getAuditVehicleList(status_id,userId);
            System.out.println("tripDetails===="+tripDetails);
            return tripDetails;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (tripDetails != null) {
                ArrayList<VehicleListTO> runSheetList = new ArrayList<VehicleListTO>();
                try {
                    JSONArray array = new JSONArray(tripDetails);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObj = array.getJSONObject(i);
                        VehicleListTO runSheet = new VehicleListTO(
                                jsonObj.getString("vehicleNumber"),
                                jsonObj.getString("vendorName"),
                                jsonObj.getString("vendorId"),
                                jsonObj.getString("vehicleId"),
                                jsonObj.getString("indentId"),
                                jsonObj.getString("consignmentId"),
                                jsonObj.getString("receiptId"),
                                jsonObj.getString("receiptDate")


                        );
                        runSheetList.add(runSheet);
                    }

                    for(int i=0 ;i<runSheetList.size();i++){
                        System.out.println("vehicleNumber==="+runSheetList.get(i).vehicleNumber);
                        System.out.println("vendor name==="+runSheetList.get(i).vendorName);
                    }
                    if (runSheetList.size() == 0) {
                        noinvoice.setVisibility(View.VISIBLE);
                        searchbar.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                    });
                }

                runSheetAdapter = new GateInVehicleListAdapter(GateInVehicleListActivity.this, runSheetList);
                lv.setAdapter(runSheetAdapter);
            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }

        }


    }













}
