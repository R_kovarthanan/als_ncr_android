package com.application.ncr;

/**
 * Created by Kova on 22-12-2020.
 */


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import androidx.appcompat.app.AppCompatActivity;
import java.util.HashMap;
public class SessionManager extends Activity {
    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    // Context
    Context _context;
    //tc
    private SharedPreferences preferences;
    private SharedPreferences.Editor tceditor;
    private String FILE_NAME = "app.Preferences";
//    private String KEY_THERMS = "KEY_THERMS";

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "HandS";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "nameW";
    // Email address (make variable public to access from outside)
    public static final String KEY_PASSWORD = "passW";
    // DesigId  (make variable public to access from outside)
    public static final String KEY_DESIG = "desigW";
    public static final String KEY_USERID = "userIdW";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

        preferences = _context.getSharedPreferences(FILE_NAME, PRIVATE_MODE);
        tceditor = preferences.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(String name, String pass, String desigId,String userId) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, name);
        // Storing password in pref
        editor.putString(KEY_PASSWORD, pass);
        // Storing designationId in pref
        editor.putString(KEY_DESIG, desigId);
        editor.putString(KEY_USERID, userId);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {

        // Check login status
        System.out.println("checking login");
        if (!this.isLoggedIn()) {
            System.out.println("not login");
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, CheckLoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            _context.startActivity(i);
        }

    }


    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        // user password
        user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));
        //user desig
        user.put(KEY_DESIG, pref.getString(KEY_DESIG, null));
        user.put(KEY_USERID, pref.getString(KEY_USERID, null));
        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        System.out.println("logging out");
        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, CheckLoginActivity.class);
        // Closing all the Activities
        i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Staring Login Activity
        _context.startActivity(i);
        finish();
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

//    public void setTherms(String value) {
//        tceditor.putString(KEY_THERMS, value);
//        tceditor.commit();
//
//    }
//
//    public String getTherms() {
//        return preferences.getString(KEY_THERMS, "NO");
//    }
}
