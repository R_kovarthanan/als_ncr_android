package com.application.ncr;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class PreDispatch extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    SessionManager session;
    DrawerLayout drawer;
    private ProgressDialog pDialog;
    String Username, userId, DesingId;
    String vendorId, indentId, vehicleId, receiptId;
    final String[] tvp_dealer_list = {"TVP/WAREHOUSE", "DEALER", "CROSS DOCK"};
    final String[] ewaybill_type_list = {"Single", "Consolidate"};
    EditText tvp_dealer_edit, ewaybill_type_edit;
    Spinner tvp_dealer_spinner, ewaybill_type_spinner, consignor_spinner, tvp_spinner;
    TextInputLayout tvp_dealer_layout, ewaybill_type_layout, ewaybill_input, consignor_layout, tvp_layout;
    TextInputEditText ewaybillNum_ET;
    EditText ewaybill_date_ET, consignor_edit, tvp_edit;
    Button next;
    String tvp, ewaybillType = null, ewaybillNum = null, ewaybillDate = null, consignorId = null, tvpId = null;
    DatePickerDialog picker;
    String type_id = null;

    ArrayList<String> consignorNameList = new ArrayList<String>();
    ArrayList<String> consignorIdList = new ArrayList<String>();
    ArrayList<String> tvpNameList = new ArrayList<String>();
    ArrayList<String> tvpIdList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pre_dispatch);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);

        Intent intent = getIntent();
        vendorId = intent.getStringExtra("vendor_id");
        vehicleId = intent.getStringExtra("vehicleId");
        indentId = intent.getStringExtra("indentId");
        receiptId = intent.getStringExtra("receiptId");

//        new checkexist().execute();

//        new getTvpList().execute();

        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Feild Executive");
        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
//        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
//        nav_dashboard.setVisible(false);

        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code

        tvp_dealer_layout = (TextInputLayout) findViewById(R.id.tvp_dealer_layout);
        tvp_dealer_spinner = (Spinner) findViewById(R.id.tvp_dealer_spinner);
        tvp_dealer_edit = (EditText) findViewById(R.id.edit1);

        ewaybill_type_layout = (TextInputLayout) findViewById(R.id.ewaybill_layout);
        ewaybill_type_spinner = (Spinner) findViewById(R.id.ewaybill_type_spinner);
        ewaybill_type_edit = (EditText) findViewById(R.id.edit2);

        ewaybillNum_ET = (TextInputEditText) findViewById(R.id.editText1);
        ewaybill_input = (TextInputLayout) findViewById(R.id.ewaybill_input);


        ewaybill_date_ET = (EditText) findViewById(R.id.ewaybillDate);
        ewaybill_date_ET.setInputType(InputType.TYPE_NULL);
        ewaybill_date_ET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(PreDispatch.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                ewaybill_date_ET.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        consignor_layout = (TextInputLayout) findViewById(R.id.consignor_layout);
        consignor_spinner = (Spinner) findViewById(R.id.consignor_spinner);
        consignor_edit = (EditText) findViewById(R.id.consignee_edit);

        tvp_layout = (TextInputLayout) findViewById(R.id.tvp_layout);
        tvp_spinner = (Spinner) findViewById(R.id.tvp_spinner);
        tvp_edit = (EditText) findViewById(R.id.tvp_edit);


        next = (Button) findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("ewaybillTypein pre audit=========" + ewaybillType);
                if (tvp.length() != 0 && tvp != "") {
                    if (ewaybillType != null) {
                        if (consignorId != null) {

                            if (ewaybillType.equals("2")) {
                                if (ewaybillNum_ET.getText().length() != 0 && ewaybillNum_ET.getText().toString() != "") {
                                    if (ewaybill_date_ET.length() != 0 && ewaybill_date_ET.getText().toString() != "") {


                                        ewaybillNum = ewaybillNum_ET.getText().toString();
                                        ewaybillDate = ewaybill_date_ET.getText().toString();

                                        //pass indent with ewaybill num and date

//                                    Intent intObj = new Intent(PreDispatch.this, DispatchActivity.class);
//                                    intObj.putExtra("vehicleId", vehicleId);
//                                    intObj.putExtra("vendor_id", vendorId);
//                                    intObj.putExtra("indentId", indentId);
//                                    intObj.putExtra("receiptId", receiptId);
//                                    intObj.putExtra("ewaybillNum", ewaybillNum);
//                                    intObj.putExtra("ewaybillDate", ewaybillDate);
//                                    intObj.putExtra("ewaybillType", ewaybillType);
//                                    intObj.putExtra("tvp", tvp);
//                                    intObj.putExtra("consignorId", consignorId);
//                                    startActivity(intObj);

                                        new savePreAuditDetails().execute();

                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Please Enter Ewaybill Expiry Date", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "Please Enter Ewaybill Number", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            } else {

                                //pass indent without ewaybill num and date
//                            Intent intObj = new Intent(PreDispatch.this, DispatchActivity.class);
//                            intObj.putExtra("vehicleId", vehicleId);
//                            intObj.putExtra("vendor_id", vendorId);
//                            intObj.putExtra("indentId", indentId);
//                            intObj.putExtra("receiptId", receiptId);
//                            intObj.putExtra("ewaybillNum", ewaybillNum);
//                            intObj.putExtra("ewaybillDate", ewaybillDate);
//                            intObj.putExtra("ewaybillType", ewaybillType);
//                            intObj.putExtra("tvp", tvp);
//                            intObj.putExtra("consignorId", consignorId);
//                            startActivity(intObj);

                                new savePreAuditDetails().execute();
                            }


                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Please Choose Consignor", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Please Choose EwayBill Type", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Please Choose Tvp/Dealer", Toast.LENGTH_LONG).show();
                        }
                    });
                }


            }

        });


        ArrayAdapter dataAdapterForSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tvp_dealer_list);
        dataAdapterForSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tvp_dealer_spinner.setAdapter(dataAdapterForSpinner);
        tvp_dealer_edit.setKeyListener(null);
        tvp_dealer_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvp_dealer_spinner.setVisibility(View.VISIBLE);
                tvp_dealer_spinner.performClick();
            }
        });

        tvp_dealer_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    tvp_dealer_spinner.setVisibility(View.VISIBLE);
                    tvp_dealer_spinner.performClick();
                } else {
                    tvp_dealer_spinner.setVisibility(View.GONE);
                }
            }
        });

        tvp_dealer_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvp_dealer_edit.setText(tvp_dealer_spinner.getSelectedItem().toString()); //this is taking the first value of the spinner by default.
                if (tvp_dealer_spinner.getSelectedItem().toString().equalsIgnoreCase("TVP/WAREHOUSE")) {
                    tvp = "1";
                    tvp_layout.setVisibility(View.VISIBLE);
                    System.out.println("tvp_dealer========" + tvp);
                    type_id = "4";
                    tvpNameList.clear();
                    tvpIdList.clear();
                    tvp_edit.setText("");
                    new getTvpList().execute();
                }
                if (tvp_dealer_spinner.getSelectedItem().toString().equalsIgnoreCase("DEALER")) {
                    tvp = "2";
                    tvp_layout.setVisibility(View.GONE);
                    System.out.println("tvp_dealer========" + tvp);
                }

                if (tvp_dealer_spinner.getSelectedItem().toString().equalsIgnoreCase("CROSS DOCK")) {
                    tvp = "3";
                    tvp_layout.setVisibility(View.VISIBLE);
                    System.out.println("tvp_dealer========" + tvp);
                    type_id = "5";
                    tvpNameList.clear();
                    tvpIdList.clear();
                    tvp_edit.setText("");
                    new getTvpList().execute();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                tvp_dealer_edit.setText("");
            }
        });


        ArrayAdapter dataAdapterForSpinner1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ewaybill_type_list);
        dataAdapterForSpinner1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ewaybill_type_spinner.setAdapter(dataAdapterForSpinner1);
        ewaybill_type_edit.setKeyListener(null);
        ewaybill_type_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ewaybill_type_spinner.setVisibility(View.VISIBLE);
                ewaybill_type_spinner.performClick();
            }
        });

        ewaybill_type_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    ewaybill_type_spinner.setVisibility(View.VISIBLE);
                    ewaybill_type_spinner.performClick();
                } else {
                    ewaybill_type_spinner.setVisibility(View.GONE);
                }
            }
        });

        ewaybill_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ewaybill_type_edit.setText(ewaybill_type_spinner.getSelectedItem().toString()); //this is taking the first value of the spinner by default.
                if (ewaybill_type_spinner.getSelectedItem().toString().equalsIgnoreCase("Consolidate")) {
                    ewaybillType = "2";
                    System.out.println("ewaybillType========" + ewaybillType);
                    ewaybill_input.setVisibility(View.VISIBLE);
                    ewaybill_date_ET.setVisibility(View.VISIBLE);
                }
                if (ewaybill_type_spinner.getSelectedItem().toString().equalsIgnoreCase("Single")) {
                    ewaybillType = "1";
                    System.out.println("ewaybillType========" + ewaybillType);
                    ewaybill_input.setVisibility(View.GONE);
                    ewaybill_date_ET.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                ewaybill_type_edit.setText("");
            }
        });


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            Intent mainIntent = new Intent(PreDispatch.this, CheckLoginActivity.class);
            startActivity(mainIntent);
        }
//            if(DesingId.equals("1042")){
//
//            }
        return true;
    }


    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        Intent intObj = new Intent(PreDispatch.this, DispatchVehicleListActivity.class);
//                intObj.putExtra("username", username);
        startActivity(intObj);
    }


    public class getConsignorList extends AsyncTask<String, String, String> {
        String getConsigneeList;

        protected void onPreExecute() {
//            pDialog = new ProgressDialog(PreDispatch.this);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
        }

        @Override
        public String doInBackground(String... params) {
            getConsigneeList = WebService.getConsignorList(receiptId);
            System.out.println("getConsigneeList====" + getConsigneeList);
            if (getConsigneeList != null) {


                try {
                    JSONArray array = new JSONArray(getConsigneeList);
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObj = array.getJSONObject(i);
                            consignorId = jsonObj.getString("consignorId");
                            System.out.println("consignorId====="+consignorId);
//                            consignorNameList.add(jsonObj.getString("consignorNameList"));
//                            consignorIdList.add(jsonObj.getString("consignorIdList"));

                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "No consignor Created in the customer contract", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "No consignor Created in the customer contract", Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            System.out.println("consignorId=== in predispatch"+consignorId);
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
//            new getProductTypeList().execute();



            //hiding aat 08-07-2021
//            ArrayAdapter<String> dataAdapterForSpinner1 = new ArrayAdapter<String>(PreDispatch.this, android.R.layout.simple_spinner_dropdown_item, consignorNameList);
//            dataAdapterForSpinner1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            consignor_spinner.setAdapter(dataAdapterForSpinner1);
//            consignor_edit.setKeyListener(null);
//            consignor_edit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    consignor_spinner.setVisibility(View.VISIBLE);
//                    consignor_spinner.performClick();
//                }
//            });
//
//            consignor_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                @Override
//                public void onFocusChange(View view, boolean b) {
//                    if (b) {
//                        consignor_spinner.setVisibility(View.VISIBLE);
//                        consignor_spinner.performClick();
//                    } else {
//                        consignor_spinner.setVisibility(View.GONE);
//                    }
//                }
//            });
//
//            consignor_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    consignor_edit.setText(consignor_spinner.getSelectedItem().toString()); //this is taking the first value of the spinner by default.
//                    consignorId = consignorIdList.get(position);
//                    System.out.println("consignorId in pre dispatch========" + consignorId);
////
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//                    // TODO Auto-generated method stub
//                    consignor_edit.setText("");
//                }
//            });

            //hiding aat 08-07-2021


        }
    }


    public class getTvpList extends AsyncTask<String, String, String> {
        String getTvpList;

        protected void onPreExecute() {
//            pDialog = new ProgressDialog(PreDispatch.this);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
        }

        @Override
        public String doInBackground(String... params) {
            getTvpList = WebService.getTvpList(receiptId, type_id);
            System.out.println("getConsigneeList====" + getTvpList);
            if (getTvpList != null) {


                try {
                    JSONArray array = new JSONArray(getTvpList);
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObj = array.getJSONObject(i);
                            tvpNameList.add(jsonObj.getString("tvpNameList"));
                            tvpIdList.add(jsonObj.getString("tvpIdList"));

                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "No tvp Created in the customer contract", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "No tvp Created in the customer contract", Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
//            new getProductTypeList().execute();
            ArrayAdapter<String> dataAdapterForSpinner10 = new ArrayAdapter<String>(PreDispatch.this, android.R.layout.simple_spinner_dropdown_item, tvpNameList);
            dataAdapterForSpinner10.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            tvp_spinner.setAdapter(dataAdapterForSpinner10);
            tvp_edit.setKeyListener(null);
            tvp_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvp_spinner.setVisibility(View.VISIBLE);
                    tvp_spinner.performClick();
                }
            });

            tvp_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b) {
                        tvp_spinner.setVisibility(View.VISIBLE);
                        tvp_spinner.performClick();
                    } else {
                        tvp_spinner.setVisibility(View.GONE);
                    }
                }
            });

            tvp_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    tvp_edit.setText(tvp_spinner.getSelectedItem().toString()); //this is taking the first value of the spinner by default.
                    tvpId = tvpIdList.get(position);
                    System.out.println("tvpId========" + tvpId);
//
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                    tvp_edit.setText("");
                }
            });
            consignorNameList.clear();
            consignorIdList.clear();
            new getConsignorList().execute();
        }
    }


    public class savePreAuditDetails extends AsyncTask<String, String, String> {

        String tripDetails = null;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(PreDispatch.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            tripDetails = WebService.savePreDispatchDetails(receiptId, consignorId, ewaybillType, ewaybillDate, ewaybillNum, tvp, tvpId);
            return tripDetails;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (tripDetails != null) {
                try {
                    JSONArray jsonArray = new JSONArray(tripDetails);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String savePreDispatchDetails = jsonObject.getString("status");
                    String exceptionMsg = jsonObject.getString("exectpion");
                    if ("1".equalsIgnoreCase(savePreDispatchDetails)) {
                        Intent intObj = new Intent(PreDispatch.this, DispatchActivity.class);
                        intObj.putExtra("vehicleId", vehicleId);
                        intObj.putExtra("vendor_id", vendorId);
                        intObj.putExtra("indentId", indentId);
                        intObj.putExtra("receiptId", receiptId);
                        intObj.putExtra("ewaybillNum", ewaybillNum);
                        intObj.putExtra("ewaybillDate", ewaybillDate);
                        intObj.putExtra("ewaybillType", ewaybillType);
                        intObj.putExtra("tvp", tvp);
                        intObj.putExtra("consignorId", consignorId);
                        startActivity(intObj);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), exceptionMsg, Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), exceptionMsg, Toast.LENGTH_LONG).show();
                            }
                        });

                    }

                } catch (final JSONException e) {
                    System.out.println("JSONException=========" + e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }

        }


    }


}
