package com.application.ncr;


import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class WebService {
//    spring.datasource.url = jdbc:mysql://throttledevnew.cudua6f07cfh.us-west-2.rds.amazonaws.com:3306/throttle_als_ncr?useSSL=false
//    spring.datasource.username = admin
//    spring.datasource.password = etsadmin321#


//    private static String RestURL = "http://demo.throttletms.com:8899/NCRServiceAPI/NCR/api/v1/";// demo
//    private static String RestURL1 = "http://demo.throttletms.com:8899/NCRServiceAPI/NCR/api/v1/";// demo

//    private static String RestURL = "http://49.207.178.125:8086/NCRServiceAPI/NCR/api/v1/";// demo local
//    private static String RestURL1 = "http://49.207.178.125:8086/NCRServiceAPI/NCR/api/v1/";//demo local

//    private static String RestURL = "http://124.123.68.18:8086/NCRServiceAPI/NCR/api/v1/";// demo local
//    private static String RestURL1 = "http://124.123.68.18:8086/NCRServiceAPI/NCR/api/v1/";//demo local

//    private static String RestURL = "http://34.216.244.63:8899/NCRServiceAPI/NCR/api/v1/";// live
//    private static String RestURL1 = "http://34.216.244.63:8899/NCRServiceAPI/NCR/api/v1/";//live

    private static String RestURL = "http://192.168.0.172:8080/NCRServiceAPI/NCR/api/v1/";// kova local
    private static String RestURL1 = "http://192.168.0.172:8080/NCRServiceAPI/NCR/api/v1/";//kova local


//    private static String RestURL = "http://demo.throttletms.com:8088/BVMServiceAPI/api/v1/";// kova local
//    private static String RestURL1 = "http://demo.throttletms.com:8088/BVMServiceAPI/api/v1/";//kova local

//    private static String RestURL = "http://demo.throttletms.com:8088/BVMServiceAPI/api/v1/";//local server
//    private static String RestURL1 = "http://demo.throttletms.com:8088/BVMServiceAPI/api/v1/";//local server

//    private static String RestURL = "http://52.66.244.61/BVMServiceAPI/api/v1/";// live
//    private static String RestURL1 = "http://52.66.244.61/BVMServiceAPI/api/v1/";// live

    private static final String ContentType = "application/json";


    public static String verifyUser(String userName, String password) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "checkLoginUser?username=" + userName + "&password=" + password;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            e.printStackTrace();
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }





    public static String getDashboardCount(String userId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getDashboardCount?userId="+userId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getAuditVehicleList(String status,String userId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getAuditVehicleList?status="+status+"&userId="+userId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getGateOutVehicleList(String status,String userId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getGateOutVehicleList?status="+status+"&userId="+userId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getInvoiceList(String receiptId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getInvoiceList?receiptId="+receiptId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String saveAuditFile(String vendor_id,String indentId, String vehicleId, String userId,String status, String obj,String receiptId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "saveAuditFile?vendorId="+vendor_id+"&indentId="+indentId+"&vehicleId="+vehicleId+"&userId="+userId+"&status="+status+"&receiptId="+receiptId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }




    public static String updateVehicleGateIn(String drivername,String drivernumber,String vehiclenumber,
                                             String userId,String obj,String status,String vendorId, String vehicleId, String indentId,String receiptId,String driverLicense) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateVehicleGateIn?vehicleNo="+vehiclenumber+"&driverName="
                    +drivername+"&mobile="+drivernumber+"&userId="+userId+"&status="+status+"&vendorId="+vendorId+"&vehicleId="+vehicleId+"&indentId="+indentId+"&receiptId="+receiptId+"&driverLicense="+driverLicense ;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");

            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String updateVehicleGateOut(String drivername,String drivernumber,String vehiclenumber,
                                             String userId,String obj,String status,String vendorId, String vehicleId, String indentId,String receiptId,String DriverLicenseNum,
                                              String sealNumber,String odometerReadings) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateVehicleGateOut?vehicleNo="+vehiclenumber+"&driverName="
                    +drivername+"&mobile="+drivernumber+"&userId="+userId+"&status="+status+"&vendorId="+vendorId+"&vehicleId="+vehicleId+"&indentId="+indentId+"&receiptId="+receiptId+"&driverLicense="+DriverLicenseNum+"&sealNumber="+sealNumber+"&odometerReading="+odometerReadings ;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");

            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String   updateDispatchDetails(String vehicleId, String vendorId, String indentId, String userId,String status,String consignmentId,String receiptId,int checkedStatus) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateDispatchDetails?userId="+userId+"&vendorId="+vendorId+"&vehicleId="+vehicleId+"&indentId="+indentId+"&status="+status+"&receiptId="+receiptId+"&checkedStatus="+checkedStatus ;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            System.out.println("exception===="+e);
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            System.out.println("exception===="+e);
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("exception===="+e);
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("exception===="+e);
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }


    public static String submitInvoice(String indentId, String vehicleId, String invNum, String gcn, String ewaybill, String consigneeId, String weight, String receiptId, String consignorId,String vendorId,String bikeQty,String scooterQty,
                                       String description,String invDate,String ewaybillValid,String tvp,String invoiceValue) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "submitInvoice?indentId="+indentId+"&vehicleId="+vehicleId+"&invNum="+invNum+"&gcn="+gcn+"&ewaybill="+ewaybill+
                    "&consignee="+consigneeId+"&weight="+weight+"&receiptId="+receiptId+"&consignorId="+consignorId+"&vendorId="+vendorId+
                    "&bikeUnit="+bikeQty+"&scotterUnit="+scooterQty+"&description="+description+"&invDate="+invDate+
                    "&ewaybillValid="+ewaybillValid+"&tvp="+tvp+"&invoiceValue="+invoiceValue;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getConsigneeList(String receiptId,String consignorId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getConsigneeList?receiptId="+receiptId+"&consignorId="+consignorId;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getConsignorList(String receiptId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getConsignorList?receiptId="+receiptId;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getTvpList(String receiptId,String typeId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getTvpList?receiptId="+receiptId+"&typeId="+typeId;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getProductTypeList() {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getProductTypeList";
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getAuditForm() {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getAuditForm";
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }




    public static String savePreDispatchDetails(String receiptId,String consignorId,String ewaybillType, String ewaybillDate,
                                                String ewaybillNum,String tvp,String tvpId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "savePreDispatchDetails?receiptId="+receiptId+"&consignorId="+consignorId+"&ewaybillType="+ewaybillType+"&ewaybillDate="+ewaybillDate+"&ewaybillNum="+ewaybillNum+
                    "&tvp="+tvp+"&tvpId="+tvpId;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }



    public static String getTvpConsigneeList(String receiptId,String consignorId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getTvpConsigneeList?receiptId="+receiptId+"&consignorId="+consignorId;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String deleteInvoice(String intendId,String receiptId,String invoiceId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "deleteInvoice?receiptId="+receiptId+"&intendId="+intendId+"&invoiceId="+invoiceId;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("DELETE");

//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }




    private static String convertStreamToString(InputStream is) {
//        RestURL = "http://192.168.51.25:8098/ThrottleService/WS/finAgg/";
        RestURL = RestURL1;
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


}

