package com.application.ncr;

public class VehicleAuditListTO {

    String vehicleNumber = null;
    String vendorName = null;
    String vendorId = null;
    String vehicleId = null;
    String indentId = null;
    String consignmentId = null;
    String receiptId = null;
    String receiptDate = null;
    String tvp_id = null;
    String ewaybill_number = null;
    String ewaybill_expiry_date = null;
    String tvp_dealer = null;
    String ewaybill_type = null;
    String origin_id = null;

    public VehicleAuditListTO(String vehicleNumber, String vendorName, String vendorId, String vehicleId, String indentId, String consignmentId, String receiptId, String receiptDate, String tvp_id, String ewaybill_number, String ewaybill_expiry_date, String tvp_dealer, String ewaybill_type, String origin_id) {
        this.vehicleNumber = vehicleNumber;
        this.vendorName = vendorName;
        this.vendorId = vendorId;
        this.vehicleId = vehicleId;
        this.indentId = indentId;
        this.consignmentId = consignmentId;
        this.receiptId = receiptId;
        this.receiptDate = receiptDate;
        this.tvp_id = tvp_id;
        this.ewaybill_number = ewaybill_number;
        this.ewaybill_expiry_date = ewaybill_expiry_date;
        this.tvp_dealer = tvp_dealer;
        this.ewaybill_type = ewaybill_type;
        this.origin_id = origin_id;
    }


    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getIndentId() {
        return indentId;
    }

    public void setIndentId(String indentId) {
        this.indentId = indentId;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }

    public String getTvp_id() {
        return tvp_id;
    }

    public void setTvp_id(String tvp_id) {
        this.tvp_id = tvp_id;
    }

    public String getEwaybill_number() {
        return ewaybill_number;
    }

    public void setEwaybill_number(String ewaybill_number) {
        this.ewaybill_number = ewaybill_number;
    }

    public String getEwaybill_expiry_date() {
        return ewaybill_expiry_date;
    }

    public void setEwaybill_expiry_date(String ewaybill_expiry_date) {
        this.ewaybill_expiry_date = ewaybill_expiry_date;
    }

    public String getTvp_dealer() {
        return tvp_dealer;
    }

    public void setTvp_dealer(String tvp_dealer) {
        this.tvp_dealer = tvp_dealer;
    }

    public String getEwaybill_type() {
        return ewaybill_type;
    }

    public void setEwaybill_type(String ewaybill_type) {
        this.ewaybill_type = ewaybill_type;
    }

    public String getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(String origin_id) {
        this.origin_id = origin_id;
    }
}
