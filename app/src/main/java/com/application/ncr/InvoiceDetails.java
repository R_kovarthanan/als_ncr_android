package com.application.ncr;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class InvoiceDetails extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    SessionManager session;
    DrawerLayout drawer;
    final Context context = this;
    String Username, userId, DesingId;
    String vendorId, indentId, vehicleId, exceptionMsg, receiptId, submitInvoiceStatus;
    TextInputEditText invNumET, gcnET, ewybillnumET, sequenceET, consigneeET, tvpET, unitsET, weight_ET, description_ET, invValueET;
    String invNum, gcn, ewaybill, sequence, consignee, tvp, units, consigneeId, productTypeId, obj, bikeQty, scooterQty, invDate, ewaybillValid, weight, description, invValue;
    EditText tvp_dealer_edit, consignee_edit, product_type_edit, scooterunit_edit, bikeunit_edit, invDate_et, ewaybill_ET;
    Spinner tvp_dealer_spinner, consignee_spinner, product_type_spinner;
    TextInputLayout tvp_dealer_layout, consignee_layout, product_type_layout, bikeUnitLayout, scooterUnitLayout,eway_bill_number_layout;
    Button submit;
    final String[] tvp_dealer_list = {"TVP", "DEALER"};
    private ProgressDialog pDialog;
    String consignorId ;
    private LinearLayout parentLinearLayout;
    Spinner pro_type_spinner;
    String ewaybillNum = null, ewaybillDate = null, ewaybillType = null, tvp_dealer = null;

    ArrayList<String> consigneeNameList = new ArrayList<String>();
    ArrayList<String> productTypeNameList = new ArrayList<String>();
    ArrayList<String> consigneeIdList = new ArrayList<String>();
    ArrayList<String> ProductTypeIdList = new ArrayList<String>();
    ArrayList<String> ProductList = new ArrayList<String>();
    ArrayList<String> unitList = new ArrayList<String>();
    DatePickerDialog picker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invoice_details);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);

        Intent intent = getIntent();
        vendorId = intent.getStringExtra("vendor_id");
        vehicleId = intent.getStringExtra("vehicleId");
        indentId = intent.getStringExtra("indentId");
        receiptId = intent.getStringExtra("receiptId");
        ewaybillNum = intent.getStringExtra("ewaybillNum");
        ewaybillDate = intent.getStringExtra("ewaybillDate");
        ewaybillType = intent.getStringExtra("ewaybillType");
        tvp_dealer = intent.getStringExtra("tvp");
        consignorId = intent.getStringExtra("consignorId");

        System.out.println("ewaybillType in inv details==="+ewaybillType);
        System.out.println("consignorId in inv details==="+consignorId);

        if(tvp_dealer.equals("2")){
        new getConsigneeList().execute();
        }
        if(tvp_dealer.equals("1") || tvp_dealer.equals("3")){
        new getTvpConsigneeList().execute();
        }




        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Feild Executive");
        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
//        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
//        nav_dashboard.setVisible(false);

        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code

        invNumET = (TextInputEditText) findViewById(R.id.editText1);
        invValueET = (TextInputEditText) findViewById(R.id.invoicevalue);
        gcnET = (TextInputEditText) findViewById(R.id.editText2);
        eway_bill_number_layout = findViewById(R.id.eway_bill_number);
        ewybillnumET = (TextInputEditText) findViewById(R.id.editText3);
        weight_ET = findViewById(R.id.editText4);
        description_ET = findViewById(R.id.editText5);

//        sequenceET = (TextInputEditText) findViewById(R.id.editText4);
//        unitsET = (TextInputEditText) findViewById(R.id.editText7);
//        consigneeET = (TextInputEditText) findViewById(R.id.editText5);
//        tvpET = (TextInputEditText) findViewById(R.id.editText6);

//        tvp_dealer_layout = (TextInputLayout) findViewById(R.id.tvp_dealer_layout);
//        tvp_dealer_spinner = (Spinner) findViewById(R.id.tvp_dealer_spinner);
//        tvp_dealer_edit = (EditText) findViewById(R.id.tvp_dealer_edit);

        consignee_layout = (TextInputLayout) findViewById(R.id.consignee_layout);
        consignee_spinner = (Spinner) findViewById(R.id.consignee_spinner);
        consignee_edit = (EditText) findViewById(R.id.consignee_edit);

        bikeUnitLayout = findViewById(R.id.bikeUnitLayout);
        bikeunit_edit = findViewById(R.id.bikeunit_edit);

        scooterUnitLayout = findViewById(R.id.scooterUnitLayout);
        scooterunit_edit = findViewById(R.id.scooterunit_edit);

        invDate_et = findViewById(R.id.invDate);
        invDate_et.setInputType(InputType.TYPE_NULL);
        invDate_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(InvoiceDetails.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                invDate_et.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        ewaybill_ET = findViewById(R.id.ewaybillDate);


        if (ewaybillType.equals("1")) {
            eway_bill_number_layout.setVisibility(View.VISIBLE);
//            ewybillnumET.setVisibility(View.VISIBLE);
            ewaybill_ET.setVisibility(View.VISIBLE);
            ewaybill_ET.setInputType(InputType.TYPE_NULL);
            ewaybill_ET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Calendar cldr = Calendar.getInstance();
                    int day = cldr.get(Calendar.DAY_OF_MONTH);
                    int month = cldr.get(Calendar.MONTH);
                    int year = cldr.get(Calendar.YEAR);
                    // date picker dialog
                    picker = new DatePickerDialog(InvoiceDetails.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    ewaybill_ET.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                }
                            }, year, month, day);
                    picker.show();
                }
            });
        }


//        product_type_layout = (TextInputLayout) findViewById(R.id.product_type_layout);
//        product_type_spinner = (Spinner) findViewById(R.id.product_type_spinner);
//        product_type_edit = (EditText) findViewById(R.id.product_type_edit);

        parentLinearLayout = (LinearLayout) findViewById(R.id.parent_linear_layout);
        pro_type_spinner = (Spinner) findViewById(R.id.pro_type_spinner);


//        ArrayAdapter dataAdapterForSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tvp_dealer_list);
//        dataAdapterForSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        tvp_dealer_spinner.setAdapter(dataAdapterForSpinner);
//        tvp_dealer_edit.setKeyListener(null);
//        tvp_dealer_edit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                tvp_dealer_spinner.setVisibility(View.VISIBLE);
//                tvp_dealer_spinner.performClick();
//            }
//        });
//
//        tvp_dealer_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if (b) {
//                    tvp_dealer_spinner.setVisibility(View.VISIBLE);
//                    tvp_dealer_spinner.performClick();
//                } else {
//                    tvp_dealer_spinner.setVisibility(View.GONE);
//                }
//            }
//        });
//
//        tvp_dealer_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                tvp_dealer_edit.setText(tvp_dealer_spinner.getSelectedItem().toString()); //this is taking the first value of the spinner by default.
//                if (tvp_dealer_spinner.getSelectedItem().toString().equalsIgnoreCase("TVP")) {
//                    tvp = "1";
//                    System.out.println("tvp_dealer========" + tvp);
//                }
//                if (tvp_dealer_spinner.getSelectedItem().toString().equalsIgnoreCase("DEALER")) {
//                    tvp = "2";
//
//                    System.out.println("tvp_dealer========" + tvp);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//                tvp_dealer_edit.setText("");
//            }
//        });


        submit = (Button) findViewById(R.id.submit);


        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Check if text controls are not empty
                JSONArray jArray = new JSONArray();
                if (invNumET.getText().length() != 0 && invNumET.getText().toString() != "") {
                    if (invDate_et.length() != 0 && invDate_et.getText().toString() != "") {
                        if (gcnET.getText().length() != 0 && gcnET.getText().toString() != "") {
//                            if (ewybillnumET.getText().length() != 0 && ewybillnumET.getText().toString() != "") {
//                                if (ewaybill_ET.length() != 0 && ewaybill_ET.getText().toString() != "") {
//                                    if (unitsET.getText().length() != 0 && unitsET.getText().toString() != "") {
//                                        if (sequenceET.getText().length() != 0 && sequenceET.getText().toString() != "") {
                            if (consigneeId.length() != 0 && consigneeId != "") {
                                if (weight_ET.length() != 0 && weight_ET.getText().toString() != "") {
                                    if (description_ET.length() != 0 && description_ET.getText().toString() != "") {
//                                                        if (tvp.length() != 0 && tvp != "") {
                                        if (bikeunit_edit.getText().length() != 0 && bikeunit_edit.getText().toString() != "") {
                                            if (scooterunit_edit.getText().length() != 0 && scooterunit_edit.getText().toString() != "") {
                                                if (invValueET.getText().length() != 0 && invValueET.getText().toString() != "") {
                                                    if (ewaybillType.equals("1")) {

                                                        if (ewybillnumET.getText().length() != 0 && ewybillnumET.getText().toString() != "" && ewybillnumET.getText().toString() != null) {
                                                            if (ewaybill_ET.length() != 0 && ewaybill_ET.getText().toString() != "") {
                                                                System.out.println("ewaybillType--------"+ewaybillType);
                                                                invNum = invNumET.getText().toString();
                                                                gcn = gcnET.getText().toString();
                                                                ewaybill = ewybillnumET.getText().toString();
                                                                bikeQty = bikeunit_edit.getText().toString();
                                                                scooterQty = scooterunit_edit.getText().toString();
                                                                invDate = invDate_et.getText().toString();
                                                                ewaybillValid = ewaybill_ET.getText().toString();
                                                                weight = weight_ET.getText().toString();
                                                                description = description_ET.getText().toString();
                                                                tvp = tvp_dealer;
                                                                invValue = invValueET.getText().toString();

                                                                System.out.println("invNumber in type1==" + invNum);
                                                                System.out.println("gcn==" + gcn);
                                                                System.out.println("ewaybill in type1==" + ewaybill);
                                                                System.out.println("tvp_dealer==" + tvp);
                                                                System.out.println("weight==" + weight);
                                                                System.out.println("description==" + description);
                                                                System.out.println("invDate==" + invDate);
                                                                System.out.println("ewaybillvalid in type1==" + ewaybillValid);
                                                                System.out.println("consigneeId==" + consigneeId);
                                                                System.out.println("consignorId==" + consignorId);
                                                                System.out.println("indentId==" + indentId);
                                                                System.out.println("receiptId==" + receiptId);
                                                                System.out.println("vendorId==" + vendorId);
                                                                System.out.println("bikeQty==" + bikeQty);
                                                                System.out.println("ScooterQty==" + scooterQty);
                                                                System.out.println("invValue==" + invValue);

//
                                                                new submitInvoice().execute();


                                                            } else {
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Toast.makeText(getApplicationContext(), "Please select ewaybill expiry Date", Toast.LENGTH_LONG).show();
                                                                    }
                                                                });
                                                            }
                                                        } else {
                                                            runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getApplicationContext(), "Please Enter ewaybill Number", Toast.LENGTH_LONG).show();
                                                                }
                                                            });
                                                        }

                                                    }else{
                                                        invNum = invNumET.getText().toString();
                                                        gcn = gcnET.getText().toString();
                                                        ewaybill = ewaybillNum;
                                                        bikeQty = bikeunit_edit.getText().toString();
                                                        scooterQty = scooterunit_edit.getText().toString();
                                                        invDate = invDate_et.getText().toString();
                                                        ewaybillValid = ewaybillDate;
                                                        weight = weight_ET.getText().toString();
                                                        description = description_ET.getText().toString();
                                                        tvp = tvp_dealer;
                                                        invValue = invValueET.getText().toString();

                                                        System.out.println("invNumber==" + invNum);
                                                        System.out.println("gcn==" + gcn);
                                                        System.out.println("ewaybill==" + ewaybill);
                                                        System.out.println("tvp_dealer==" + tvp);
                                                        System.out.println("weight==" + weight);
                                                        System.out.println("description==" + description);
                                                        System.out.println("invDate==" + invDate);
                                                        System.out.println("ewaybillvalid==" + ewaybillValid);
                                                        System.out.println("consigneeId==" + consigneeId);
                                                        System.out.println("consignorId==" + consignorId);
                                                        System.out.println("indentId==" + indentId);
                                                        System.out.println("receiptId==" + receiptId);
                                                        System.out.println("vendorId==" + vendorId);
                                                        System.out.println("bikeQty==" + bikeQty);
                                                        System.out.println("ScooterQty==" + scooterQty);
                                                        System.out.println("invValue==" + invValue);

//
                                                    new submitInvoice().execute();
                                                    }


//                                    try {
//                                        int count = parentLinearLayout.getChildCount() - 1;
//                                        for (int i = 0; i < count; i++) {
//                                            JSONObject productTypeId = new JSONObject();
//                                            final View row = parentLinearLayout.getChildAt(i);
//                                            EditText editTextResult = (TextInputEditText) row.findViewById(R.id.units);
//                                            Spinner Spinner = row.findViewById(R.id.pro_type_spinner);
//                                            int pos = Spinner.getSelectedItemPosition();
//                                            String proId = ProductTypeIdList.get(pos);
//                                            String result = editTextResult.getText().toString();
//                                            System.out.println("productId==" + proId + ",units==" + result);
//                                            productTypeId.put("productDetails", proId + "~" + result);
//                                            jArray.put(productTypeId);
//                                        }
//
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                    System.out.println("json===" + jArray);
//                                        obj = jArray.toString();



                                                } else {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Toast.makeText(getApplicationContext(), "Please Enter Invoice Value", Toast.LENGTH_LONG).show();
                                                        }
                                                    });

                                                }

                                            } else {
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getApplicationContext(), "scooter quantity cant be empty", Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                            }


                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Bike quantity cant be empty", Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }

//                                                        } else {
//                                                            runOnUiThread(new Runnable() {
//                                                                @Override
//                                                                public void run() {
//                                                                    Toast.makeText(getApplicationContext(), "choose tvp or dealer", Toast.LENGTH_LONG).show();
//                                                                }
//                                                            });
//                                                        }


                                    }//eway bill date
                                    else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Please Enter Description", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                }//inv date
                                else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "Please Enter weight", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }


                            }//des is empty

                            else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Please choose consignee", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
//                                }//weight is empty
//                                else {
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(getApplicationContext(), "Please Enter ewaybill expried date ", Toast.LENGTH_LONG).show();
//                                        }
//                                    });
//                                }


//                            }//consignee is empty
//                            else {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        Toast.makeText(getApplicationContext(), "Please Enter eway bill number ", Toast.LENGTH_LONG).show();
//                                    }
//                                });
//                            }
                        }//mobilenumber is empty
                        else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Please Enter gcn  Number", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }//If vehicle number  text control is empty
                    else {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Please Invoice Date", Toast.LENGTH_LONG).show();
                            }
                        });


                    }//If  drivername text control is empty
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Please Enter Invoice Number", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


    }

    public void onAddField(View v) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.fied, null);
        Spinner Spinner = rowView.findViewById(R.id.pro_type_spinner);
        ArrayAdapter<String> dataAdapterForSpinner3 = new ArrayAdapter<String>(InvoiceDetails.this, android.R.layout.simple_spinner_dropdown_item, productTypeNameList);
        dataAdapterForSpinner3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner.setAdapter(dataAdapterForSpinner3);
        Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                productTypeId = ProductTypeIdList.get(position).toString();
                ProductList.add(productTypeId);
                System.out.println("selecteditemId======" + productTypeId);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Add the new row before the add field button.
        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
    }

    public void onDelete(View v) {
        parentLinearLayout.removeView((View) v.getParent());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            Intent mainIntent = new Intent(InvoiceDetails.this, CheckLoginActivity.class);
            startActivity(mainIntent);
        }
//            if(DesingId.equals("1042")){
//
//            }
        return true;
    }


    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }


    public class submitInvoice extends AsyncTask<String, String, String> {

        String tripDetails = null;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(InvoiceDetails.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            tripDetails = WebService.submitInvoice(indentId, vehicleId, invNum, gcn, ewaybill, consigneeId, weight, receiptId, consignorId, vendorId, bikeQty, scooterQty, description, invDate, ewaybillValid, tvp,invValue);
            return tripDetails;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (tripDetails != null) {
                try {
                    JSONArray jsonArray = new JSONArray(tripDetails);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    submitInvoiceStatus = jsonObject.getString("status");
                    exceptionMsg = jsonObject.getString("ExepctionOccr");
                    if ("1".equalsIgnoreCase(submitInvoiceStatus)) {
                        Intent intObj = new Intent(InvoiceDetails.this, DispatchActivity.class);
                        intObj.putExtra("vehicleId", vehicleId);
                        intObj.putExtra("vendor_id", vendorId);
                        intObj.putExtra("indentId", indentId);
                        intObj.putExtra("receiptId", receiptId);
                        intObj.putExtra("ewaybillNum", ewaybillNum);
                        intObj.putExtra("ewaybillDate", ewaybillDate);
                        intObj.putExtra("ewaybillType", ewaybillType);
                        intObj.putExtra("tvp", tvp);
                        intObj.putExtra("consignorId", consignorId);
                        startActivity(intObj);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), exceptionMsg, Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), exceptionMsg, Toast.LENGTH_LONG).show();
                            }
                        });

                    }

                } catch (final JSONException e) {
                    System.out.println("JSONException=========" + e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }

        }


    }

    public class getConsigneeList extends AsyncTask<String, String, String> {
        String getConsigneeList;

        protected void onPreExecute() {
            pDialog = new ProgressDialog(InvoiceDetails.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        public String doInBackground(String... params) {
            getConsigneeList = WebService.getConsigneeList(receiptId,consignorId);
            System.out.println("getConsigneeList====" + getConsigneeList);
            if (getConsigneeList != null) {


                try {
                    JSONArray array = new JSONArray(getConsigneeList);
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObj = array.getJSONObject(i);
                            consigneeNameList.add(jsonObj.getString("consigneeNameList"));
                            consigneeIdList.add(jsonObj.getString("consigneeIdList"));

                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "No consignee in the customer contract", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "No consignee in the customer contract", Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
//            new getProductTypeList().execute();
            ArrayAdapter<String> dataAdapterForSpinner1 = new ArrayAdapter<String>(InvoiceDetails.this, android.R.layout.simple_spinner_dropdown_item, consigneeNameList);
            dataAdapterForSpinner1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            consignee_spinner.setAdapter(dataAdapterForSpinner1);
            consignee_edit.setKeyListener(null);
            consignee_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    consignee_spinner.setVisibility(View.VISIBLE);
                    consignee_spinner.performClick();
                }
            });

            consignee_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b) {
                        consignee_spinner.setVisibility(View.VISIBLE);
                        consignee_spinner.performClick();
                    } else {
                        consignee_spinner.setVisibility(View.GONE);
                    }
                }
            });

            consignee_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    consignee_edit.setText(consignee_spinner.getSelectedItem().toString()); //this is taking the first value of the spinner by default.
                    consigneeId = consigneeIdList.get(position);
                    System.out.println("consigneeId========" + consigneeId);
//
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                    consignee_edit.setText("");
                }
            });


        }
    }

    public class getTvpConsigneeList extends AsyncTask<String, String, String> {
        String getConsigneeList;

        protected void onPreExecute() {
            pDialog = new ProgressDialog(InvoiceDetails.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        public String doInBackground(String... params) {
            getConsigneeList = WebService.getTvpConsigneeList(receiptId,consignorId);
            System.out.println("getConsigneeList====" + getConsigneeList);
            if (getConsigneeList != null) {


                try {
                    JSONArray array = new JSONArray(getConsigneeList);
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObj = array.getJSONObject(i);
                            consigneeNameList.add(jsonObj.getString("consigneeNameList"));
                            consigneeIdList.add(jsonObj.getString("consigneeIdList"));

                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "No consignee in the customer contract", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "No consignee in the customer contract", Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
//            new getProductTypeList().execute();
            ArrayAdapter<String> dataAdapterForSpinner1 = new ArrayAdapter<String>(InvoiceDetails.this, android.R.layout.simple_spinner_dropdown_item, consigneeNameList);
            dataAdapterForSpinner1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            consignee_spinner.setAdapter(dataAdapterForSpinner1);
            consignee_edit.setKeyListener(null);
            consignee_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    consignee_spinner.setVisibility(View.VISIBLE);
                    consignee_spinner.performClick();
                }
            });

            consignee_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b) {
                        consignee_spinner.setVisibility(View.VISIBLE);
                        consignee_spinner.performClick();
                    } else {
                        consignee_spinner.setVisibility(View.GONE);
                    }
                }
            });

            consignee_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    consignee_edit.setText(consignee_spinner.getSelectedItem().toString()); //this is taking the first value of the spinner by default.
                    consigneeId = consigneeIdList.get(position);
                    System.out.println("consigneeId========" + consigneeId);
//
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                    consignee_edit.setText("");
                }
            });


        }
    }

    public class getProductTypeList extends AsyncTask<String, String, String> {
        String getProductTypeList;

        protected void onPreExecute() {
            pDialog = new ProgressDialog(InvoiceDetails.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        public String doInBackground(String... params) {
            getProductTypeList = WebService.getProductTypeList();
            System.out.println("getProductTypeList====" + getProductTypeList);
            try {
                JSONArray array = new JSONArray(getProductTypeList);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObj = array.getJSONObject(i);
                    productTypeNameList.add(jsonObj.getString("productTypeNameList"));
                    ProductTypeIdList.add(jsonObj.getString("ProductTypeIdList"));

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
//            new getProductTypeList().execute();
            ArrayAdapter<String> dataAdapterForSpinner2 = new ArrayAdapter<String>(InvoiceDetails.this, android.R.layout.simple_spinner_dropdown_item, productTypeNameList);
            dataAdapterForSpinner2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            pro_type_spinner.setAdapter(dataAdapterForSpinner2);
            pro_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    productTypeId = ProductTypeIdList.get(position).toString();
                    ProductList.add(productTypeId);
                    System.out.println("selecteditemId======" + productTypeId);
                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


        }
    }


    @Override
    public void onBackPressed() {
        Intent intObj = new Intent(InvoiceDetails.this, DispatchVehicleListActivity.class);
//                intObj.putExtra("username", username);
        startActivity(intObj);
    }


}
