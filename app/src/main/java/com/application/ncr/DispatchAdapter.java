package com.application.ncr;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DispatchAdapter extends BaseAdapter {
    Context context;
    ArrayList<DispatchDetailsTO> runSheetList;
    ArrayList<DispatchDetailsTO> runSheetListDisplayed;
    private ProgressDialog pDialog;
    int itPosition = 0;
    TextView invoiceNum, gcn, ewaybill, location, consignee, tvp_dealer, bike_units, scooter_units;
    Button edit;
    String intendId, receiptId, invoiceId;
    private DispatchAdapter adapter;
    Dialog dialog;


    public DispatchAdapter(Context context, ArrayList<DispatchDetailsTO> runSheetList, String intendId, String receiptId) {
        this.context = context;
        this.runSheetList = runSheetList;
        this.runSheetListDisplayed = runSheetList;
        this.intendId = intendId;
        this.receiptId = receiptId;
        this.adapter = this;

    }

    @Override
    public int getCount() {
        return runSheetListDisplayed.size();
    }

    @Override
    public Object getItem(int i) {
        return runSheetListDisplayed.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);


        convertView = mInflater.inflate(R.layout.dispatch_adapter, null);

        invoiceNum = (TextView) convertView.findViewById(R.id.invoiceNum);
        gcn = (TextView) convertView.findViewById(R.id.gcn);
        ewaybill = (TextView) convertView.findViewById(R.id.ewaybill);
        location = (TextView) convertView.findViewById(R.id.location);
        consignee = (TextView) convertView.findViewById(R.id.consignee);
        tvp_dealer = (TextView) convertView.findViewById(R.id.tvp_dealer);
        bike_units = (TextView) convertView.findViewById(R.id.bike_units);
        scooter_units = (TextView) convertView.findViewById(R.id.scooter_units);
        edit = convertView.findViewById(R.id.edit);
        edit.setTag(position);

        edit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int pos = (Integer) v.getTag();
                DispatchDetailsTO OrderClicked = runSheetList.get(pos);
                itPosition = pos;
                invoiceId = OrderClicked.getInv_id();
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_alert_dialouge);
                dialog.setCancelable(false);
                Button yesButton = (Button) dialog.findViewById(R.id.yes);
                Button noButton = (Button) dialog.findViewById(R.id.no);
                Switch toggle = dialog.findViewById(R.id.switch2);
                toggle.setVisibility(View.GONE);
                yesButton.setText("Delete");
                noButton.setText("cancel");
                TextView title = (TextView) dialog.findViewById(R.id.title);
                TextView message = (TextView) dialog.findViewById(R.id.message);
                title.setText("Alert");
                message.setText("Are you sure need to delete this invoice");
                yesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new deleteInvoice().execute();
                        runSheetListDisplayed.remove(itPosition);
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        DispatchDetailsTO runSheet = runSheetListDisplayed.get(position);
        invoiceNum.setText(runSheet.getInvoiceNum());
        gcn.setText(runSheet.getGcn());
        ewaybill.setText(runSheet.getEway_bill_num());
        location.setText(runSheet.getLocation());
        consignee.setText(runSheet.getConsignee());
        if (runSheet.getTvp_dealer().equals("1")) {
            tvp_dealer.setText("TVP");
        }
        if (runSheet.getTvp_dealer().equals("2")) {
            tvp_dealer.setText("DEALER");
        }
        if (runSheet.getTvp_dealer().equals("3")) {
            tvp_dealer.setText("CROSS DOCK");
        }
        bike_units.setText(runSheet.getBikeUnits());
        scooter_units.setText(runSheet.getScooterUnits());


        return convertView;
    }


    public class deleteInvoice extends AsyncTask<String, Void, Void> {
        String obj = null;
        String jsonStr = null;

        @Override
        protected void onPreExecute() {
//            pDialog = new ProgressDialog(context);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            System.out.println("intendId==" + intendId + ",,,receiptId==" + receiptId + ",,,invoiceId==" + invoiceId);
            jsonStr = WebService.deleteInvoice(intendId, receiptId, invoiceId);
            System.out.println("json_string_output====" + jsonStr);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
            if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String exceptionMsg = jsonObject.getString("expOccur");
                    if (exceptionMsg.equalsIgnoreCase("N")) {
//                        dialog.dismiss();
//                        Intent intent = new Intent(context, DispatchActivity.class);
//                        context.startActivity(intent);
                        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //context.refreshInbox();
                                Toast.makeText(context, "Deleted successfully", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //context.refreshInbox();
                                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                } catch (final JSONException e) {
                    ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //context.refreshInbox();
                            Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {
                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //context.refreshInbox();
                        Toast.makeText(context, "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });

            }

        }


    }


}