package com.application.ncr;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

public class GateInVehicleListAdapter extends BaseAdapter
//        implements Filterable
{
    Context context;
    ArrayList<VehicleListTO> runSheetList;
    ArrayList<VehicleListTO> runSheetListDisplayed;
    private ProgressDialog pDialog;
    int itPosition = 0;
    TextView vendorName, vehicleNumber,receiptDate;
    Button gateIn;
    String vendorId,indentId,vehicleId,receiptId,vehNum;


    public GateInVehicleListAdapter(Context context, ArrayList<VehicleListTO> runSheetList) {
        this.context = context;
//        this.runSheetList = runSheetList;
        this.runSheetListDisplayed = runSheetList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);


            convertView = mInflater.inflate(R.layout.gatein_vehicle_list_adapter, null);

            vehicleNumber = (TextView) convertView.findViewById(R.id.vehicle_number);
            vendorName = (TextView) convertView.findViewById(R.id.vendor_name);
            receiptDate = (TextView) convertView.findViewById(R.id.receipt_date);
            gateIn = (Button) convertView.findViewById(R.id.button1);
            gateIn.setTag(position);

            VehicleListTO runSheet = runSheetListDisplayed.get(position);
            vendorName.setText(runSheet.getVendorName());
            vehicleNumber.setText(runSheet.getVehicleNumber());
            receiptDate.setText(runSheet.getReceiptDate());


            gateIn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int pos = (Integer) v.getTag();
                    VehicleListTO OrderClicked = runSheetListDisplayed.get(pos);
                    itPosition = pos;
                    vendorId = OrderClicked.getVendorId();
                    vehicleId = OrderClicked.getVehicleId();
                    indentId = OrderClicked.getIndentId();
                    receiptId = OrderClicked.getReceiptId();
                    vehNum = OrderClicked.getVehicleNumber();
                    Intent intent = new Intent(context, GateIn.class);
                    intent.putExtra("vendor_id", vendorId);
                    intent.putExtra("vehicleId", vehicleId);
                    intent.putExtra("indentId", indentId);
                    intent.putExtra("receiptId", receiptId);
                    intent.putExtra("vehicleNum", vehNum);

                    context.startActivity(intent);


                }
            });





        return convertView;
    }

    @Override
    public int getCount() {
        return runSheetListDisplayed.size();
    }

    @Override
    public Object getItem(int i) {
        return runSheetListDisplayed.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }



//    @Override
//    public Filter getFilter() {
//        Filter filter = new Filter() {
//
//            @SuppressWarnings("unchecked")
//            @Override
//            protected void publishResults(CharSequence constraint, FilterResults results) {
//
//                runSheetListDisplayed = (ArrayList<VehicleListTO>) results.values; // has the filtered values
//                notifyDataSetChanged();  // notifies the data with new filtered values
//            }
//
//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
//                ArrayList<VehicleListTO> FilteredArrList = new ArrayList<VehicleListTO>();
//
//                if (runSheetList == null) {
//                    runSheetList = new ArrayList<VehicleListTO>(runSheetListDisplayed); // saves the original data in mOriginalValues
//                }
//
//                /********
//                 *
//                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
//                 *  else does the Filtering and returns FilteredArrList(Filtered)
//                 *
//                 ********/
//                if (constraint == null || constraint.length() == 0) {
//
//                    // set the Original result to return
//                    results.count = runSheetList.size();
//                    results.values = runSheetList;
//                } else {
//                    constraint = constraint.toString().toLowerCase();
//                    for (int i = 0; i < runSheetList.size(); i++) {
//                        String data = runSheetList.get(i).vehicleNumber;
//                        if (data.toLowerCase().contains(constraint.toString())) {
//                            FilteredArrList.add(new VehicleListTO(runSheetList.get(i).getVehicleNumber(),
//                                    runSheetList.get(i).getVendorName(), runSheetList.get(i).getVendorId(),
//                                    runSheetList.get(i).getVehicleId(),runSheetList.get(i).getIndentId(),runSheetList.get(i).getConsignmentId(),runSheetList.get(i).getReceiptId()));
//                        }
//                    }
//                    // set the Filtered result to return
//                    results.count = FilteredArrList.size();
//                    results.values = FilteredArrList;
//                }
//                return results;
//            }
//        };
//        return filter;
//    }
}
