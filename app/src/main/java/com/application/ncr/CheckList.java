package com.application.ncr;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class CheckList extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    SessionManager session;
    String Username, userId, DesingId;
    DrawerLayout drawer;
    private ProgressDialog pDialog;
    Dialog dialog;
    final Context context = this;
    String vendorId, indentId, vehicleId, receiptId,obj;

    LinearLayout podCapture;
    GridView gridView;
    int MY_CAMERA_PERMISSION_CODE = 100, CAMERA_REQUEST = 1888;
    byte[] CustomerSignature = null;
    ArrayList imageString = new ArrayList();
    int count = 0;
    ArrayList<ColorItem> CamraTakeImgList = new ArrayList<>();
    String billImage = null;
    Button photoButton, submit;
    CheckListAdapter runSheetAdapter;
    ArrayList<CheckListTO> invoiceList = new ArrayList<CheckListTO>();
    ListView lv;
    Boolean ischecked = false;
    TabLayout tabLayout;
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.check_list);

        new getAuditForm().execute();

        Intent intent = getIntent();
        vendorId = intent.getStringExtra("vendor_id");
        vehicleId = intent.getStringExtra("vehicleId");
        indentId = intent.getStringExtra("indentId");
        receiptId = intent.getStringExtra("receiptId");

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);

        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Feild Executive");
        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code


        submit = (Button) findViewById(R.id.Submit);
        lv = (ListView) findViewById(R.id.homeListView);

        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.viewpager);
        tabLayout.addTab(tabLayout.newTab().setText("Pre loading"));
        tabLayout.addTab(tabLayout.newTab().setText("Post Loading"));
//        tabLayout.addTab(tabLayout.newTab().setText("During Loading"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final MyAdapter adapter = new MyAdapter(this, getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


//        podCapture = (LinearLayout) findViewById(R.id.podCapture);
//        photoButton = (Button) findViewById(R.id.button1);
//        gridView = (GridView) findViewById(R.id.gridView);


//        photoButton.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View v) {
//                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
//
//                } else {
//                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
//                }
//            }
//        });


        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Check if text controls are not empty
                for(int i=0; i<invoiceList.size();i++){
                    System.out.println("auditid=="+invoiceList.get(i).getAuditId());
                    System.out.println("auditData=="+invoiceList.get(i).getAuditData());
                    System.out.println("isChecked=="+invoiceList.get(i).getIsChecked());
                }
                JSONArray jArray = new JSONArray();
                try {
                    for (int i = 0; i < invoiceList.size(); i++) {
                        JSONObject auditId = new JSONObject();
                        if (invoiceList.get(i).getIsChecked().equals(false)) {
                            auditId.put("auditId", invoiceList.get(i).getAuditId());
                            jArray.put(auditId);
                        }

                    }
                    System.out.println("jarry===="+jArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                obj=jArray.toString();
                new saveAuditFile().execute();
            }
        });


    }


    public class saveAuditFile extends AsyncTask<String, String, String> {

        String updateStatus = null;
        String status_id = "2";

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(CheckList.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            updateStatus = WebService.saveAuditFile(vendorId, indentId, vehicleId, userId, status_id, obj, receiptId);
            return updateStatus;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }


            if (updateStatus != null) {

                try {
                    JSONArray jsonArray = new JSONArray(updateStatus);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");

                    System.out.println("statusMsg===" + status);
                    if ("1".equals(status)) {
                        Intent intObj = new Intent(CheckList.this, Dashboard.class);
                        startActivity(intObj);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Succesfully updated", Toast.LENGTH_LONG).show();
                            }
                        });

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Failed,please try again", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } catch (final JSONException e) {
                    System.out.println("JSONException=========" + e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Something went wrong,please try again", Toast.LENGTH_LONG).show();
                        }
                    });
//                    statusTV.setText("Invalid Data");

                }

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }


        }


    }

    public class getAuditForm extends AsyncTask<String, String, String> {

        String getAuditForm = null;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(CheckList.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            getAuditForm = WebService.getAuditForm();
            System.out.println("getAuditForm===="+getAuditForm);
            return getAuditForm;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (getAuditForm != null) {

                try {
                    JSONArray array = new JSONArray(getAuditForm);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObj = array.getJSONObject(i);
                        CheckListTO runSheet = new CheckListTO(
                                jsonObj.getString("audit_id"),
                                jsonObj.getString("auditData"), ischecked
                        );

                        invoiceList.add(runSheet);
                    }

//                    for(int i=0; i<invoiceList.size();i++){
//                        System.out.println("auditid=="+invoiceList.get(i).getAuditId());
//                        System.out.println("auditData=="+invoiceList.get(i).getAuditData());
//                        System.out.println("isChecked=="+invoiceList.get(i).getIsChecked());
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("exeption====" + e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                    });
                }

                runSheetAdapter = new CheckListAdapter(CheckList.this, invoiceList);
                lv.setAdapter(runSheetAdapter);
            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }


        }


    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            session.logoutUser();
        }

        return true;
    }

    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {


            try {
                count++;
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                CustomerSignature = bos.toByteArray();
                billImage = Base64.encodeToString(CustomerSignature, Base64.NO_WRAP);
                imageString.add(billImage);
                ColorItem select = new ColorItem();
                select.setBitmap(bitmap);
                select.setColorName("Pic" + " " + count);
                CamraTakeImgList.add(select);
                SelectedImageAdapter seletimg = new SelectedImageAdapter(CheckList.this, CamraTakeImgList);
                gridView.setAdapter(seletimg);

            } catch (NullPointerException e) {

                e.printStackTrace();

            }

        }
    }
}
