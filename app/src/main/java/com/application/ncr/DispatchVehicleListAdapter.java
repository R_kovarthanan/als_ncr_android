package com.application.ncr;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

public class DispatchVehicleListAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<VehicleAuditListTO> runSheetList;
    ArrayList<VehicleAuditListTO> runSheetListDisplayed;
    private ProgressDialog pDialog;
    int itPosition = 0;
    TextView vendorName, vehicleNumber,receiptDate;
    Button dispatch;
    String vendorId,indentId,vehicleId,receiptId,ewaybillNum,ewaybillDate,ewaybillType,tvp,consignorId;


    public DispatchVehicleListAdapter(Context context, ArrayList<VehicleAuditListTO> runSheetList) {
        this.context = context;
        this.runSheetList = runSheetList;
        this.runSheetListDisplayed = runSheetList;
    }

    @Override
    public int getCount() {
        return runSheetListDisplayed.size();
    }

    @Override
    public Object getItem(int i) {
        return runSheetListDisplayed.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);


            convertView = mInflater.inflate(R.layout.dispatch_vehicle_list_adapter, null);

            vehicleNumber = (TextView) convertView.findViewById(R.id.vehicle_number);
            vendorName = (TextView) convertView.findViewById(R.id.vendor_name);
            receiptDate = (TextView) convertView.findViewById(R.id.receipt_date);
            dispatch= (Button) convertView.findViewById(R.id.button1);
            dispatch.setTag(position);

            VehicleAuditListTO runSheet = runSheetListDisplayed.get(position);
            vendorName.setText(runSheet.getVendorName());
            vehicleNumber.setText(runSheet.getVehicleNumber());
            receiptDate.setText(runSheet.getReceiptDate());


            dispatch.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int pos = (Integer) v.getTag();
                    VehicleAuditListTO OrderClicked = runSheetListDisplayed.get(pos);
                    itPosition = pos;
                    System.out.println("tvp_id=="+OrderClicked.getTvp_id()+" ,tvp_dealer="+OrderClicked.getTvp_dealer() +
                            " ,ewaybilltype= "+OrderClicked.getEwaybill_type() +" ,origin_id== "+OrderClicked.getOrigin_id() );

                    if(!OrderClicked.getTvp_id().equals("null") && !OrderClicked.getTvp_dealer() .equals("null") && !OrderClicked.getEwaybill_type() .equals("null")
//                    && OrderClicked.getOrigin_id() != null
                    ){



                        vendorId = OrderClicked.getVendorId();
                        vehicleId = OrderClicked.getVehicleId();
                        indentId = OrderClicked.getIndentId();
                        receiptId = OrderClicked.getReceiptId();
                        ewaybillNum = OrderClicked.getEwaybill_number();
                        ewaybillDate = OrderClicked.getEwaybill_expiry_date();
                        ewaybillType =OrderClicked.getEwaybill_type();
                        tvp = OrderClicked.getTvp_dealer();
                        consignorId = OrderClicked.getOrigin_id();
                        Intent intent = new Intent(context, DispatchActivity.class);
//                    Intent intent = new Intent(context, DispatchActivity.class);
                        intent.putExtra("vendor_id", vendorId);
                        intent.putExtra("vehicleId", vehicleId);
                        intent.putExtra("indentId", indentId);
                        intent.putExtra("receiptId", receiptId);
                        intent.putExtra("ewaybillNum", ewaybillNum);
                        intent.putExtra("ewaybillDate", ewaybillDate);
                        intent.putExtra("ewaybillType", ewaybillType);
                        intent.putExtra("tvp", tvp);
                        intent.putExtra("consignorId", consignorId);
//                    intent.putExtra("consignmentId", OrderClicked.getConsignmentId());
                        context.startActivity(intent);

                    }else{
                        System.out.println("im going to pre dispatch");
                    vendorId = OrderClicked.getVendorId();
                    vehicleId = OrderClicked.getVehicleId();
                    indentId = OrderClicked.getIndentId();
                    receiptId = OrderClicked.getReceiptId();
                    Intent intent = new Intent(context, PreDispatch.class);
//                    Intent intent = new Intent(context, DispatchActivity.class);
                    intent.putExtra("vendor_id", vendorId);
                    intent.putExtra("vehicleId", vehicleId);
                    intent.putExtra("indentId", indentId);
                    intent.putExtra("receiptId", receiptId);
//                    intent.putExtra("consignmentId", OrderClicked.getConsignmentId());
                    context.startActivity(intent);
                    }


                }
            });





        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                runSheetListDisplayed = (ArrayList<VehicleAuditListTO>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<VehicleAuditListTO> FilteredArrList = new ArrayList<VehicleAuditListTO>();

                if (runSheetList == null) {
                    runSheetList = new ArrayList<VehicleAuditListTO>(runSheetListDisplayed); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = runSheetList.size();
                    results.values = runSheetList;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < runSheetList.size(); i++) {
                        String data = runSheetList.get(i).getVehicleNumber();
                        if (data.toLowerCase().contains(constraint.toString())) {
                            FilteredArrList.add(new VehicleAuditListTO(runSheetList.get(i).getVehicleNumber(),
                                    runSheetList.get(i).getVendorName(), runSheetList.get(i).getVendorId(),
                                    runSheetList.get(i).getVehicleId(),runSheetList.get(i).getIndentId(),
                                    runSheetList.get(i).getConsignmentId(),runSheetList.get(i).getReceiptId(),
                                    runSheetList.get(i).getReceiptDate(),runSheetList.get(i).getTvp_id(),
                                    runSheetList.get(i).getEwaybill_number(),runSheetList.get(i).getEwaybill_expiry_date(),
                                    runSheetList.get(i).getTvp_dealer(),runSheetList.get(i).getEwaybill_type(),runSheetList.get(i).getOrigin_id()));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }
}
