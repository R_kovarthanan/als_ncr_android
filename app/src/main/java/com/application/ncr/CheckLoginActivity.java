package com.application.ncr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CheckLoginActivity extends AppCompatActivity {
    SessionManager session;
    ProgressBar ProgeressBar;
    TextInputEditText userNameET, passWordET;
    TextView statusTV;
    TextInputLayout userTI, passTI;
    Button loginButton;
    SharedPreferences sharedpreferences;
    String editTextPassword, editTextUsername;
    String  exceptionMsg, userId, desigId, signIn = "0";
    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        session = new SessionManager(getApplicationContext());
        ProgeressBar = (ProgressBar) findViewById(R.id.progressBar1);

        userNameET = (TextInputEditText) findViewById(R.id.editText1);
        passWordET = (TextInputEditText) findViewById(R.id.editText2);
        statusTV = (TextView) findViewById(R.id.tv_result);
        loginButton = (Button) findViewById(R.id.login);
        userTI = findViewById(R.id.userNameInput);
        passTI = findViewById(R.id.passwordInput);


        userNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
//                    System.out.println("iam here");
                    if (userNameET.getText().length() == 0 && userNameET.getText().toString().equals("")) {
                        System.out.println("iam here 1");
                        userTI.setErrorEnabled(true);
                        userTI.setError("error");


                    } else {
                        userTI.setError(null);
                        userTI.setErrorEnabled(false);
                    }

//                    saveThisItem(txtClientID.getText().toString(), "name", txtName.getText().toString());
                }
            }
        });

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Check if text controls are not empty

                if (userNameET.getText().length() != 0 && userNameET.getText().toString() != "") {
                    if (passWordET.getText().length() != 0 && passWordET.getText().toString() != "") {
                        editTextUsername = userNameET.getText().toString();
                        editTextPassword = passWordET.getText().toString();
                        statusTV.setText("");

//                		session set value
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        String n = editTextUsername;
                        String pw = editTextPassword;
                        editor.putString("Name", n);
                        editor.putString("pass", pw);
                        editor.commit();
                        new verifyUser().execute();

//                        Intent intObj = new Intent(CheckLoginActivity.this, Dashboard.class);
//                        startActivity(intObj);

                    }
                    //If Password text control is empty
                    else {
                        statusTV.setText("Please enter Password");
                    }
                    //If Username text control is empty
                } else {
                    statusTV.setText("Please enter Username");
                }
            }
        });
    }


    public class verifyUser extends AsyncTask<String, Void, Void> {
        String obj = null;
        String jsonStr = null;

        @Override
        //Make Progress Bar visible
        protected void onPreExecute() {
            ProgeressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... params) {

            jsonStr = WebService.verifyUser(editTextUsername, editTextPassword);
            System.out.println("json_string_output====" + jsonStr);


            if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    exceptionMsg = jsonObject.getString("status");

                    System.out.println("status===" + exceptionMsg);
                    if ("Success".equalsIgnoreCase(exceptionMsg)) {
                        userId = jsonObject.getString("UserId");
                        desigId = jsonObject.getString("DesigId");
                        System.out.println("userId=="+userId+" desigId=="+desigId);
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), exceptionMsg, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } catch (final JSONException e) {
                    System.out.println("JSONException=========" + e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Something went wrong,please try again", Toast.LENGTH_LONG).show();
                        }
                    });
//                    statusTV.setText("Invalid Data");

                }
            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
//                statusTV.setText("Could't get data from the server");

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgeressBar.setVisibility(View.GONE);
            if ("Success".equalsIgnoreCase(exceptionMsg)) {
                session.createLoginSession(editTextUsername, editTextPassword, desigId, userId);
                Intent intObj = new Intent(CheckLoginActivity.this, MainActivity.class);
                startActivity(intObj);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);
        finishAffinity();
    }


}



