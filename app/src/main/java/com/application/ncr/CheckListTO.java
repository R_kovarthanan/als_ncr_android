package com.application.ncr;

public class CheckListTO {

    String auditId = null;
    String auditData = null;
    Boolean isChecked = null;

    public CheckListTO(String auditId, String auditData,Boolean isChecked) {
        this.auditId = auditId;
        this.auditData = auditData;
        this.isChecked = isChecked;
    }

    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    public String getAuditData() {
        return auditData;
    }

    public void setAuditData(String auditData) {
        this.auditData = auditData;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }
}
